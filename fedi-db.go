package main

import (
	"context"
	"crypto/rsa"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/streams/vocab"
	"github.com/google/uuid"
)

// pub.Database implementation

type fediDatabase struct {
	lockedURLs map[string]bool

	// lock protects all above fields and their contents
	lock sync.Mutex

	privKey *rsa.PrivateKey
	once    sync.Once
}

var errLockHeld = errors.New("lock already taken on this URL")
var errNotFound = errors.New("not found")

func (f *fediDatabase) init() {
	f.lockedURLs = make(map[string]bool)
}

func (f *fediDatabase) Lock(ctx context.Context, id *url.URL) error {
	f.once.Do(f.init)

	s := id.String()

	for try := 0; try < 10; try++ {
		f.lock.Lock()
		if !f.lockedURLs[s] {
			f.lockedURLs[s] = true
			f.lock.Unlock()
			return nil
		}
		f.lock.Unlock()
		time.Sleep(time.Millisecond)
	}

	return errLockHeld
}

func (f *fediDatabase) Unlock(ctx context.Context, id *url.URL) error {
	f.lock.Lock()
	delete(f.lockedURLs, id.String())
	f.lock.Unlock()

	return nil
}

func (*fediDatabase) InboxContains(ctx context.Context, inbox *url.URL, id *url.URL) (contains bool, err error) {
	var count int
	err = db.QueryRowContext(ctx, `SELECT COUNT(*) FROM "federation_inbox_contents" WHERE "inbox" = $1 AND "id" = $2`, inbox.String(), id.String()).Scan(&count)
	return count != 0, err
}

func (*fediDatabase) GetInbox(ctx context.Context, inboxIRI *url.URL) (inbox vocab.ActivityStreamsOrderedCollectionPage, err error) {
	page := streams.NewActivityStreamsOrderedCollectionPage()
	idProperty := streams.NewJSONLDIdProperty()
	idProperty.Set(inboxIRI)
	page.SetJSONLDId(idProperty)
	return page, nil
}

func (*fediDatabase) SetInbox(ctx context.Context, inbox vocab.ActivityStreamsOrderedCollectionPage) error {
	inboxID := inbox.GetJSONLDId().Get()
	for it := inbox.GetActivityStreamsOrderedItems().Begin(); it != inbox.GetActivityStreamsOrderedItems().End(); it = it.Next() {
		objectID := it.GetIRI()
		_, err := db.ExecContext(ctx, `INSERT INTO "federation_inbox_contents" ("inbox", "id") VALUES ($1, $2)`, inboxID.String(), objectID.String())
		if err != nil {
			return err
		}
	}

	return nil
}

func (*fediDatabase) Owns(ctx context.Context, id *url.URL) (owns bool, err error) {
	return id.Scheme == "https" && id.Host == "gw2.lubar.me", nil
}

func (*fediDatabase) OutboxForInbox(ctx context.Context, inboxIRI *url.URL) (outboxIRI *url.URL, err error) {
	if strings.HasSuffix(inboxIRI.Path, "/_inbox") && inboxIRI.Path != "/_inbox" {
		outboxIRI = new(url.URL)
		*outboxIRI = *inboxIRI
		outboxIRI.Path = strings.TrimSuffix(outboxIRI.Path, "/_inbox") + "/_outbox"
		return
	}
	return nil, errors.New("not implemented: pub.Database.InboxForOutbox")
}

func (*fediDatabase) InboxForOutbox(ctx context.Context, outboxIRI *url.URL) (inboxIRI *url.URL, err error) {
	if strings.HasSuffix(outboxIRI.Path, "/_outbox") && outboxIRI.Path != "/_outbox" {
		inboxIRI = new(url.URL)
		*inboxIRI = *outboxIRI
		inboxIRI.Path = strings.TrimSuffix(inboxIRI.Path, "/_outbox") + "/_inbox"
		return
	}
	return nil, errors.New("not implemented: pub.Database.InboxForOutbox")
}

func (*fediDatabase) ActorForOutbox(ctx context.Context, outboxIRI *url.URL) (actorIRI *url.URL, err error) {
	if strings.HasSuffix(outboxIRI.Path, "/_outbox") && outboxIRI.Path != "/_outbox" {
		actorIRI = new(url.URL)
		*actorIRI = *outboxIRI
		actorIRI.Path = strings.TrimSuffix(actorIRI.Path, "/_outbox")
		return
	}
	return nil, errors.New("not implemented: pub.Database.ActorForOutbox")
}

func (*fediDatabase) ActorForInbox(ctx context.Context, inboxIRI *url.URL) (actorIRI *url.URL, err error) {
	if strings.HasSuffix(inboxIRI.Path, "/_inbox") && inboxIRI.Path != "/_inbox" {
		actorIRI = new(url.URL)
		*actorIRI = *inboxIRI
		actorIRI.Path = strings.TrimSuffix(actorIRI.Path, "/_inbox")
		return
	}
	return nil, errors.New("not implemented: pub.Database.ActorForInbox")
}

func (f *fediDatabase) Exists(ctx context.Context, id *url.URL) (exists bool, err error) {
	if id.Scheme == "https" && id.Host == "gw2.lubar.me" {
		if _, err = f.Get(ctx, id); err == nil {
			exists = true
			return
		}
	}

	var count int
	err = db.QueryRowContext(ctx, `SELECT COUNT(*) FROM "federation_remote_objects" WHERE "id" = $1`, id.String()).Scan(&count)
	return count != 0, err
}

func (f *fediDatabase) Get(ctx context.Context, id *url.URL) (value vocab.Type, err error) {
	f.once.Do(f.init)

	if id.Scheme == "https" && id.Host == "gw2.lubar.me" {
		path := id.Path[1:]
		if path == "log" {
			service := streams.NewActivityStreamsService()
			idProperty := streams.NewJSONLDIdProperty()
			idProperty.Set(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/log",
			})
			service.SetJSONLDId(idProperty)

			nameProperty := streams.NewActivityStreamsNameProperty()
			nameProperty.AppendXMLSchemaString(fullGuildName() + " Guild Log")
			service.SetActivityStreamsName(nameProperty)

			preferredNameProperty := streams.NewActivityStreamsPreferredUsernameProperty()
			preferredNameProperty.SetXMLSchemaString("log")
			service.SetActivityStreamsPreferredUsername(preferredNameProperty)

			inboxProperty := streams.NewActivityStreamsInboxProperty()
			inboxProperty.SetIRI(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/log/_inbox",
			})
			service.SetActivityStreamsInbox(inboxProperty)

			outboxProperty := streams.NewActivityStreamsOutboxProperty()
			outboxProperty.SetIRI(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/log/_outbox",
			})
			service.SetActivityStreamsOutbox(outboxProperty)

			followersProperty := streams.NewActivityStreamsFollowersProperty()
			followersProperty.SetIRI(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/log/_followers",
			})
			service.SetActivityStreamsFollowers(followersProperty)

			followingProperty := streams.NewActivityStreamsFollowingProperty()
			followingProperty.SetIRI(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/log/_following",
			})
			service.SetActivityStreamsFollowing(followingProperty)

			iconProperty := streams.NewActivityStreamsIconProperty()
			iconImage := streams.NewActivityStreamsImage()

			iconMediaType := streams.NewActivityStreamsMediaTypeProperty()
			iconMediaType.Set("image/png")
			iconImage.SetActivityStreamsMediaType(iconMediaType)

			iconUrl := streams.NewActivityStreamsUrlProperty()
			iconUrl.AppendXMLSchemaAnyURI(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/logo.png",
			})
			iconImage.SetActivityStreamsUrl(iconUrl)

			iconProperty.AppendActivityStreamsImage(iconImage)
			service.SetActivityStreamsIcon(iconProperty)

			summaryProperty := streams.NewActivityStreamsSummaryProperty()
			summaryProperty.AppendXMLSchemaString(`<p>` + fullGuildName() + ` <a href="https://www.guildwars2.com/">Guild Wars 2</a> guild log.</p><p>Contact <a href="https://mastodon.lubar.me/@ben">@ben</a> if this breaks or for more information.</p>`)
			service.SetActivityStreamsSummary(summaryProperty)

			tagProperty := streams.NewActivityStreamsTagProperty()
			benMention := streams.NewActivityStreamsMention()

			benMentionName := streams.NewActivityStreamsNameProperty()
			benMentionName.AppendXMLSchemaString("@ben")
			benMention.SetActivityStreamsName(benMentionName)

			benMentionHref := streams.NewActivityStreamsHrefProperty()
			benMentionHref.Set(&url.URL{
				Scheme: "https",
				Host:   "mastodon.lubar.me",
				Path:   "/@ben",
			})
			benMention.SetActivityStreamsHref(benMentionHref)

			tagProperty.AppendActivityStreamsMention(benMention)
			service.SetActivityStreamsTag(tagProperty)

			attachmentProperty := streams.NewActivityStreamsAttachmentProperty()

			treasuryField := streams.NewActivityStreamsObject()

			treasuryFieldType := streams.NewJSONLDTypeProperty()
			treasuryFieldType.AppendXMLSchemaString("PropertyValue")
			treasuryField.SetJSONLDType(treasuryFieldType)

			treasuryFieldName := streams.NewActivityStreamsNameProperty()
			treasuryFieldName.AppendXMLSchemaString("Treasury")
			treasuryField.SetActivityStreamsName(treasuryFieldName)

			treasuryField.GetUnknownProperties()["value"] = `<a href="https://gw2.lubar.me/treasury" rel="me nofollow noopener" target="_blank"><span class="invisible">https://</span><span class="">gw2.lubar.me/treasury</span></a>`

			_ = attachmentProperty.AppendType(&propertyValueWrapper{treasuryField})

			bankField := streams.NewActivityStreamsObject()

			bankFieldType := streams.NewJSONLDTypeProperty()
			bankFieldType.AppendXMLSchemaString("PropertyValue")
			bankField.SetJSONLDType(bankFieldType)

			bankFieldName := streams.NewActivityStreamsNameProperty()
			bankFieldName.AppendXMLSchemaString("Bank")
			bankField.SetActivityStreamsName(bankFieldName)

			bankField.GetUnknownProperties()["value"] = `<a href="https://gw2.lubar.me/bank" rel="me nofollow noopener" target="_blank"><span class="invisible">https://</span><span class="">gw2.lubar.me/bank</span></a>`

			_ = attachmentProperty.AppendType(bankField)

			service.SetActivityStreamsAttachment(attachmentProperty)

			maf := streams.NewActivityStreamsManuallyApprovesFollowersProperty()
			maf.Set(false)
			service.SetActivityStreamsManuallyApprovesFollowers(maf)

			pubKeyObj := streams.NewW3IDSecurityV1PublicKey()
			pubKeyID := streams.NewJSONLDIdProperty()
			pubKeyID.Set(&url.URL{
				Scheme:   "https",
				Host:     "gw2.lubar.me",
				Path:     "/log",
				Fragment: keyID,
			})
			pubKeyObj.SetJSONLDId(pubKeyID)
			pubKeyOwner := streams.NewW3IDSecurityV1OwnerProperty()
			pubKeyOwner.Set(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/log",
			})
			pubKeyObj.SetW3IDSecurityV1Owner(pubKeyOwner)
			pubKeyPem := streams.NewW3IDSecurityV1PublicKeyPemProperty()
			pubKeyPem.Set(pubKey)
			pubKeyObj.SetW3IDSecurityV1PublicKeyPem(pubKeyPem)
			pubKeyProperty := streams.NewW3IDSecurityV1PublicKeyProperty()
			pubKeyProperty.AppendW3IDSecurityV1PublicKey(pubKeyObj)
			service.SetW3IDSecurityV1PublicKey(pubKeyProperty)

			discoverableProperty := streams.NewTootDiscoverableProperty()
			discoverableProperty.Set(true)
			service.SetTootDiscoverable(discoverableProperty)

			return service, nil
		}

		if strings.HasPrefix(path, "members/") {
			memberName := path[len("members/"):]
			for _, m := range guildMembers {
				if strings.EqualFold(m.Account, memberName) && !guildLogBlacklist[m.Account] {
					return makeMemberActor(m)
				}
			}

			rows, err := db.QueryContext(ctx, `SELECT DISTINCT "user" FROM "guild_log_entries" WHERE LOWER("user") = LOWER($1)`, memberName)
			if err != nil {
				return nil, err
			}
			defer rows.Close()

			var foundName string
			var found bool
			for rows.Next() {
				if err = rows.Scan(&foundName); err != nil {
					return nil, err
				}

				if guildLogBlacklist[foundName] {
					found = false
					break
				}

				found = true
			}

			if err = rows.Err(); err != nil {
				return nil, err
			}

			if found {
				return makeMemberActor(guildMember{
					Account: foundName,
				})
			}
		}

		if strings.HasSuffix(path, "/_following") {
			return f.Following(ctx, &url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   strings.TrimSuffix(id.Path, "/_following"),
			})
		}
		if strings.HasSuffix(path, "/_followers") {
			return f.Followers(ctx, &url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   strings.TrimSuffix(id.Path, "/_followers"),
			})
		}

		if idString, ok := prefixSuffix(path, "_item/", "/_emoji"); ok {
			if itemID, err := strconv.Atoi(idString); err == nil {
				return getItemEmoji(itemID)
			}
		}
		if idString, ok := prefixSuffix(path, "_upgrade/", "/_emoji"); ok {
			if itemID, err := strconv.Atoi(idString); err == nil {
				return getUpgradeEmoji(itemID)
			}
		}

		if idString, ok := prefixSuffix(path, "log/entry/", "/_create"); ok {
			var id int
			var published time.Time
			var logType, entries string
			err := db.QueryRow(`SELECT "id", "type", "time", "entries" FROM "guild_log_combined" WHERE "id" = $1`, idString).Scan(&id, &logType, &published, &entries)
			if err == sql.ErrNoRows {
				return nil, errNotFound
			}
			if err != nil {
				return nil, err
			}
			return makeLogCreateActivity(id, logType, published, []byte(entries)), nil
		} else if strings.HasPrefix(path, "log/entry/") && strings.Count(path, "/") == 2 {
			var id int
			var published time.Time
			var logType, entries string
			err := db.QueryRow(`SELECT "id", "type", "time", "entries" FROM "guild_log_combined" WHERE "id" = $1`, path[len("log/entry/"):]).Scan(&id, &logType, &published, &entries)
			if err == sql.ErrNoRows {
				return nil, errNotFound
			}
			return makeLogObject(id, logType, published, []byte(entries))
		}
	}

	return f.get(ctx, id)
}

func (f *fediDatabase) get(ctx context.Context, id *url.URL) (vocab.Type, error) {
	var payload []byte
	err := db.QueryRowContext(ctx, `SELECT "payload" FROM "federation_remote_objects" WHERE "id" = $1`, id.String()).Scan(&payload)
	if err == sql.ErrNoRows {
		return nil, errNotFound
	}
	if err != nil {
		return nil, err
	}
	var payloadMap map[string]interface{}
	err = json.Unmarshal(payload, &payloadMap)
	if err != nil {
		return nil, err
	}
	payloadMap["@context"] = []interface{}{
		"https://www.w3.org/ns/activitystreams",
		map[string]string{
			"toot":  "http://joinmastodon.org/ns#",
			"Emoji": "toot:Emoji",
		},
	}
	return streams.ToType(ctx, payloadMap)
}

type propertyValueWrapper struct {
	vocab.ActivityStreamsObject
}

func (a *propertyValueWrapper) JSONLDContext() map[string]string {
	m := a.ActivityStreamsObject.JSONLDContext()

	m["https://schema.org#"] = "schema"
	m["schema:PropertyValue"] = "PropertyValue"
	m["schema:value"] = "value"

	return m
}

func (f *fediDatabase) Create(ctx context.Context, asType vocab.Type) error {
	id := asType.GetJSONLDId().Get()
	if id.Scheme == "https" && id.Host == "gw2.lubar.me" {
		if strings.HasPrefix(id.Path, "/log/entry/") {
			return nil
		}
	}

	m, err := asType.Serialize()
	if err != nil {
		return err
	}
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	_, err = db.ExecContext(ctx, `INSERT INTO "federation_remote_objects" ("id", "payload") VALUES($1, $2)`, id.String(), b)
	if err != nil {
		log.Println(id, err)
	}
	return err
}

func (f *fediDatabase) Update(ctx context.Context, asType vocab.Type) error {
	id := asType.GetJSONLDId().Get()

	m, err := asType.Serialize()
	if err != nil {
		return err
	}
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	res, err := db.ExecContext(ctx, `UPDATE "federation_remote_objects" SET "payload" = $1 WHERE "id" = $2`, b, id.String())
	if err != nil {
		return err
	}
	n, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if n == 0 {
		_, err = db.ExecContext(ctx, `INSERT INTO "federation_remote_objects" ("id", "payload") VALUES($1, $2)`, id.String(), b)
	}
	return err
}

func (f *fediDatabase) Delete(ctx context.Context, id *url.URL) error {
	if id.Scheme == "https" && id.Host == "gw2.lubar.me" {
		ok, err := f.Exists(ctx, id)
		if err != nil {
			return err
		}
		if !ok {
			return nil
		}
		return fmt.Errorf("DB.Delete should not have been called for local object %q", id)
	}

	_, err := db.ExecContext(ctx, `DELETE FROM "federation_remote_objects" WHERE "id" = $1`, id.String())
	return err
}

func (*fediDatabase) GetOutbox(ctx context.Context, outboxIRI *url.URL) (outbox vocab.ActivityStreamsOrderedCollectionPage, err error) {
	outbox = streams.NewActivityStreamsOrderedCollectionPage()
	id := streams.NewJSONLDIdProperty()
	id.Set(outboxIRI)
	outbox.SetJSONLDId(id)
	return
}

func (*fediDatabase) SetOutbox(ctx context.Context, outbox vocab.ActivityStreamsOrderedCollectionPage) error {
	// not saved
	return nil
}

func (*fediDatabase) NewID(ctx context.Context, t vocab.Type) (id *url.URL, err error) {
	if t.GetJSONLDId() != nil && t.GetJSONLDId().Get() != nil {
		return t.GetJSONLDId().Get(), nil
	}
	return &url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/_ephemeral/" + uuid.New().String(),
	}, nil
}

func (f *fediDatabase) Followers(ctx context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	id := *actorIRI
	id.Path += "/_followers"
	c, err := f.get(ctx, &id)
	return removeBlocked(requireCollection(c, err, &id))
}

func (f *fediDatabase) Following(ctx context.Context, actorIRI *url.URL) (following vocab.ActivityStreamsCollection, err error) {
	id := *actorIRI
	id.Path += "/_following"
	c, err := f.get(ctx, &id)
	return requireCollection(c, err, &id)
}

func (*fediDatabase) Liked(ctx context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	return nil, errors.New("not implemented: pub.Database.Liked")
}
