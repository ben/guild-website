FROM golang:1.17

WORKDIR /

COPY go.mod go.sum /src/

RUN cd /src && go mod download

COPY *.go /src/

RUN cd /src && go build -o /usr/local/bin/guild-website

COPY static /static
COPY data-cache /data-cache

# ensure the templates compile
RUN env DATA_SOURCE_NAME= /usr/local/bin/guild-website

VOLUME /data

ARG BUILD_NUMBER=unknown
ENV BUILD_NUMBER=$BUILD_NUMBER

ENTRYPOINT ["guild-website"]
