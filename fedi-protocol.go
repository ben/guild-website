package main

import (
	"context"
	"errors"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-fed/activity/pub"
	"github.com/go-fed/activity/streams/vocab"
)

// pub.FederatingProtocol implementation

type fediProtocol struct{}

func (*fediProtocol) PostInboxRequestBodyHook(ctx context.Context, r *http.Request, act pub.Activity) (context.Context, error) {
	return ctx, nil
}

func (*fediProtocol) AuthenticatePostInbox(ctx context.Context, w http.ResponseWriter, r *http.Request) (ctx_ context.Context, authenticated bool, err error) {
	if strings.HasPrefix(r.URL.Path, "/members/") {
		// pretend to accept it
		w.WriteHeader(http.StatusAccepted)

		return ctx, false, nil
	}

	return ctx, true, nil
}

func (*fediProtocol) Blocked(ctx context.Context, actorIRIs []*url.URL) (blocked bool, err error) {
	for _, iri := range actorIRIs {
		if isBlocked(iri) {
			return true, nil
		}
	}

	return false, nil
}

func (*fediProtocol) FederatingCallbacks(ctx context.Context) (wrapped pub.FederatingWrappedCallbacks, other []interface{}, err error) {
	wrapped.OnFollow = pub.OnFollowAutomaticallyAccept
	return
}

func (*fediProtocol) DefaultCallback(ctx context.Context, activity pub.Activity) error {
	return errors.New("not implemented: pub.FederatingProtocol.DefaultCallback")
}

func (*fediProtocol) MaxInboxForwardingRecursionDepth(ctx context.Context) int {
	return 0 // TODO: is this okay?
}

func (*fediProtocol) MaxDeliveryRecursionDepth(ctx context.Context) int {
	return 0 // TODO: is this okay?
}

func (*fediProtocol) FilterForwarding(ctx context.Context, potentialRecipients []*url.URL, a pub.Activity) (filteredRecipients []*url.URL, err error) {
	return potentialRecipients, nil
}

func (*fediProtocol) GetInbox(ctx context.Context, r *http.Request) (vocab.ActivityStreamsOrderedCollectionPage, error) {
	return nil, errors.New("_inbox does not support GET requests")
}
