package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

type guildData struct {
	Info     guildInfo
	Stash    []*guildStashVault
	Storage  []*upgradeCount
	Teams    struct{}
	Treasury []*guildTreasuryItem
	Upgrades []int
}

var guildDataCache struct {
	data       *guildData
	lastUpdate time.Time
	sync.Mutex
}

const guildCacheDuration = 1 * time.Minute

func getGuildName() (name, tag string) {
	guildDataCache.Lock()

	if guildDataCache.data != nil {
		// guild names don't change, so stale cache is fine
		name = guildDataCache.data.Info.Name
		tag = guildDataCache.data.Info.Tag
		guildDataCache.Unlock()
		return
	}

	guildDataCache.Unlock()

	guild, err := getGuildData()
	if err != nil {
		// selfish defaults
		name = "Lubar"
		tag = "Ben"
		return
	}

	name = guild.Info.Name
	tag = guild.Info.Tag
	return
}

func fullGuildName() string {
	name, tag := getGuildName()
	return name + " [" + tag + "]"
}

func getGuildData() (*guildData, error) {
	guildDataCache.Lock()
	defer guildDataCache.Unlock()

	if guildDataCache.data != nil && time.Since(guildDataCache.lastUpdate) < guildCacheDuration {
		return guildDataCache.data, nil
	}

	var data guildData
	var err error
	doGuildAPIRequest(&err, &data.Info, "")
	doGuildAPIRequest(&err, &data.Stash, "/stash")
	doGuildAPIRequest(&err, &data.Storage, "/storage")
	//doGuildAPIRequest(&err, &data.Teams, "/teams")
	doGuildAPIRequest(&err, &data.Treasury, "/treasury")
	doGuildAPIRequest(&err, &data.Upgrades, "/upgrades")
	if err != nil {
		return nil, err
	}
	if err = data.clean(); err != nil {
		return nil, err
	}
	guildDataCache.data = &data
	guildDataCache.lastUpdate = time.Now()
	return &data, nil
}

func doGuildAPIRequest(pErr *error, data interface{}, suffix string) {
	// already hit an error on a previous request
	if *pErr != nil {
		return
	}

	req, err := http.NewRequest("GET", "https://api.guildwars2.com/v2/guild/"+guildID+suffix, nil)
	if err != nil {
		*pErr = fmt.Errorf("get guild data %q: %w", suffix, err)
		return
	}

	req.Header.Set("Authorization", "Bearer "+leaderAPIKey)
	req.Header.Set("X-Schema-Version", arbitraryAPIVersionDate)
	req.Header.Set("User-Agent", "LubarGW2/"+os.Getenv("BUILD_NUMBER")+" (+https://gw2.lubar.me/)")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		*pErr = fmt.Errorf("get guild data %q", suffix, err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		*pErr = fmt.Errorf("guild data %q request failed with status: %q", suffix, resp.Status)
		return
	}

	if err := json.NewDecoder(resp.Body).Decode(data); err != nil {
		*pErr = fmt.Errorf("get guild data %q: %w", suffix, err)
	}
}

type guildInfo struct {
	Level      int    `json:"level"`
	Experience int    `json:"-"`
	MOTD       string `json:"motd"`

	Influence int `json:"influence"`
	Aetherium int `json:"aetherium"`
	Resonance int `json:"resonance"`
	Favor     int `json:"favor"`

	MemberCount    int `json:"member_count"`
	MemberCapacity int `json:"member_capacity"`

	ID   string `json:"id"`
	Name string `json:"name"`
	Tag  string `json:"tag"`

	AetheriumRate time.Duration `json:"-"`
	AetheriumMax  int           `json:"-"`
	FavorMax      int           `json:"-"`
}

type guildStashVault struct {
	UpgradeID int           `json:"upgrade_id"`
	Upgrade   *guildUpgrade `json:"-"`
	Size      int           `json:"size"`
	Coins     int64         `json:"coins"`
	CoinsFmt  string        `json:"-"`
	Note      string        `json:"note"`
	Inventory []*itemCount  `json:"inventory"`
}

type itemCount struct {
	ItemID int      `json:"id"`
	Item   itemInfo `json:"-"`
	Count  int      `json:"count"`
}

type upgradeCount struct {
	UpgradeID int           `json:"id"`
	Upgrade   *guildUpgrade `json:"-"`
	Count     int           `json:"count"`
}

type guildTreasuryItem struct {
	ItemID      int                     `json:"item_id"`
	Item        itemInfo                `json:"-"`
	Count       int                     `json:"count"`
	NeededBy    []*guildTreasuryUpgrade `json:"needed_by"`
	TotalNeeded int                     `json:"-"`
}

func (i *guildTreasuryItem) TotalRemaining() int {
	return i.TotalNeeded - i.Count
}

type guildTreasuryUpgrade struct {
	UpgradeID int           `json:"upgrade_id"`
	Count     int           `json:"count"`
	Upgrade   *guildUpgrade `json:"-"`
}

func (d *guildData) clean() error {
	for _, f := range []func() error{
		d.cleanInfo,
		d.cleanStash,
		d.cleanStorage,
		d.cleanTeams,
		d.cleanTreasury,
		d.cleanUpgrades,
	} {
		if err := f(); err != nil {
			return err
		}
	}

	return nil
}

func (d *guildData) cleanInfo() error {
	// nothing to clean
	return nil
}

func (d *guildData) cleanStash() error {
	for _, v := range d.Stash {
		v.Upgrade = guildUpgrades[v.UpgradeID]
		v.CoinsFmt = formatCoins(v.Coins)

		for _, i := range v.Inventory {
			if i == nil {
				continue
			}

			item, err := getItemInfo(i.ItemID)
			if err != nil {
				return err
			}

			i.Item = item
		}
	}

	return nil
}

func (d *guildData) cleanStorage() error {
	for _, i := range d.Storage {
		upgrade, ok := guildUpgrades[i.UpgradeID]
		if !ok {
			return fmt.Errorf("unknown guild upgrade in storage: %d", i.UpgradeID)
		}

		i.Upgrade = upgrade
	}

	return nil
}

func (d *guildData) cleanTeams() error {
	// not yet implemented or needed
	return nil
}

func (d *guildData) cleanTreasury() error {
	haveUpgrade := make(map[int]bool)
	for _, u := range d.Upgrades {
		haveUpgrade[u] = true
	}

	for i := 0; i < len(d.Treasury); i++ {
		t := d.Treasury[i]

		item, err := getItemInfo(t.ItemID)
		if err != nil {
			return err
		}

		t.Item = item

		for j := 0; j < len(t.NeededBy); j++ {
			n := t.NeededBy[j]
			n.Upgrade = guildUpgrades[n.UpgradeID]

			if n.Upgrade.RequiredLevel > d.Info.Level {
				log.Printf("API_BROKEN upgrade %d %q needs %d of item %d %q, but also requires guild level %d, and Lubar is only level %d", n.UpgradeID, n.Upgrade.Name, n.Count, t.ItemID, t.Item.Name, n.Upgrade.RequiredLevel, d.Info.Level)
				t.NeededBy = append(t.NeededBy[:j], t.NeededBy[j+1:]...)
				j--
				continue
			}

			haveAllReqs := true
			for _, p := range n.Upgrade.Prerequisites {
				if !haveUpgrade[p] {
					req := guildUpgrades[p]
					log.Printf("API_BROKEN upgrade %d %q needs %d of item %d %q, but also requires guild upgrade %d %q, which is not unlocked by Lubar", n.UpgradeID, n.Upgrade.Name, n.Count, t.ItemID, t.Item.Name, p, req.Name)
					haveAllReqs = false
					break
				}
			}

			if !haveAllReqs {
				t.NeededBy = append(t.NeededBy[:j], t.NeededBy[j+1:]...)
				j--
				continue
			}

			t.TotalNeeded += n.Count
		}

		if t.TotalNeeded == 0 {
			log.Printf("API_BROKEN treasury shows item %d %q, but no upgrades require it [see above for possible reason]", t.ItemID, t.Item.Name)
			d.Treasury = append(d.Treasury[:i], d.Treasury[i+1:]...)
			i--
			continue
		}
	}

	return nil
}

func (d *guildData) cleanUpgrades() error {
	for _, u := range d.Upgrades {
		d.Info.Experience += guildUpgrades[u].Experience
	}

	aetheriumRates := []time.Duration{
		60 * time.Second,
		50 * time.Second,
		40 * time.Second,
		30 * time.Second,
		25 * time.Second,
		20 * time.Second,
		15 * time.Second,
	}

	aetheriumCapacities := []int{
		500,
		1500,
		3000,
		5000,
		10000,
		15000,
		25000,
	}

	d.Info.AetheriumRate = aetheriumRates[d.countUpgrades("AccumulatingCurrency", "Mining Rate")]
	d.Info.AetheriumMax = aetheriumCapacities[d.countUpgrades("AccumulatingCurrency", "Aetherium Capacity")]
	d.Info.FavorMax = 6000

	return nil
}

func (d *guildData) countUpgrades(typ, name string) int {
	var total int
	for _, id := range d.Upgrades {
		u := guildUpgrades[id]
		if u.Type == typ && strings.Contains(u.Name, name) {
			total++
		}
	}

	return total
}

func init() {
	// no data if we're just testing templates
	if os.Getenv("DATA_SOURCE_NAME") == "" {
		return
	}

	if _, err := getGuildData(); err != nil {
		log.Println("initial guild data fetch failed!", err)
	}
}
