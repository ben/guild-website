package main

import (
	"net/url"
	"strings"

	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/streams/vocab"
)

func makeMemberActor(m guildMember) (vocab.Type, error) {
	actor := streams.NewActivityStreamsPerson()

	idProperty := streams.NewJSONLDIdProperty()
	idProperty.Set(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/members/" + strings.ToLower(m.Account),
	})
	actor.SetJSONLDId(idProperty)

	nameProperty := streams.NewActivityStreamsNameProperty()
	nameProperty.AppendXMLSchemaString(m.Account)
	actor.SetActivityStreamsName(nameProperty)

	inboxProperty := streams.NewActivityStreamsInboxProperty()
	inboxProperty.SetIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/members/" + m.Account + "/_inbox",
	})
	actor.SetActivityStreamsInbox(inboxProperty)

	outboxProperty := streams.NewActivityStreamsOutboxProperty()
	outboxProperty.SetIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/members/" + m.Account + "/_outbox",
	})
	actor.SetActivityStreamsOutbox(outboxProperty)

	urlProperty := streams.NewActivityStreamsUrlProperty()
	urlProperty.AppendXMLSchemaAnyURI(m.FediverseURL)
	actor.SetActivityStreamsUrl(urlProperty)

	followersProperty := streams.NewActivityStreamsFollowersProperty()
	followersProperty.SetIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/members/" + m.Account + "/_followers",
	})
	actor.SetActivityStreamsFollowers(followersProperty)

	followingProperty := streams.NewActivityStreamsFollowingProperty()
	followingProperty.SetIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/members/" + m.Account + "/_following",
	})
	actor.SetActivityStreamsFollowing(followingProperty)

	preferredNameProperty := streams.NewActivityStreamsPreferredUsernameProperty()
	preferredNameProperty.SetXMLSchemaString(strings.ToLower(strings.Replace(strings.Replace(m.Account, ".", "", -1), " ", "_", -1)))
	actor.SetActivityStreamsPreferredUsername(preferredNameProperty)

	if m.Fediverse != "" {
		// TODO: fix this when go-fed supports movedTo natively.
		actor.GetUnknownProperties()["movedTo"] = m.FediverseURL.String()
	}

	// TODO: fix this when go-fed supports endpoints.sharedInbox natively.
	actor.GetUnknownProperties()["endpoints"] = map[string]interface{}{
		"sharedInbox": "https://gw2.lubar.me/members/_shared/_inbox",
	}

	if m.Region != "" {
		place := streams.NewActivityStreamsPlace()
		placeName := streams.NewActivityStreamsNameProperty()
		switch m.Region {
		case "EU":
			placeName.AppendXMLSchemaString("Europe")
		case "NA":
			placeName.AppendXMLSchemaString("North America")
		default:
			placeName.AppendXMLSchemaString(m.Region)
		}
		place.SetActivityStreamsName(placeName)
		location := streams.NewActivityStreamsLocationProperty()
		location.AppendActivityStreamsPlace(place)
		actor.SetActivityStreamsLocation(location)
	}

	pubKeyObj := streams.NewW3IDSecurityV1PublicKey()
	pubKeyID := streams.NewJSONLDIdProperty()
	pubKeyID.Set(&url.URL{
		Scheme:   "https",
		Host:     "gw2.lubar.me",
		Path:     "/members/" + strings.ToLower(m.Account),
		Fragment: keyID,
	})
	pubKeyObj.SetJSONLDId(pubKeyID)
	pubKeyOwner := streams.NewW3IDSecurityV1OwnerProperty()
	pubKeyOwner.Set(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/members/" + strings.ToLower(m.Account),
	})
	pubKeyObj.SetW3IDSecurityV1Owner(pubKeyOwner)
	pubKeyPem := streams.NewW3IDSecurityV1PublicKeyPemProperty()
	pubKeyPem.Set(pubKey)
	pubKeyObj.SetW3IDSecurityV1PublicKeyPem(pubKeyPem)
	pubKeyProperty := streams.NewW3IDSecurityV1PublicKeyProperty()
	pubKeyProperty.AppendW3IDSecurityV1PublicKey(pubKeyObj)
	actor.SetW3IDSecurityV1PublicKey(pubKeyProperty)

	discoverableProperty := streams.NewTootDiscoverableProperty()
	discoverableProperty.Set(false)
	actor.SetTootDiscoverable(discoverableProperty)

	return actor, nil
}
