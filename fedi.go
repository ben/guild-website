package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"database/sql"
	"encoding/json"
	"encoding/pem"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-fed/activity/pub"
	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/streams/vocab"
)

func initPrivateKey() *rsa.PrivateKey {
	b, err := ioutil.ReadFile("data/private_key.der")
	if os.IsNotExist(err) {
		key, err := rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			panic(err)
		}
		err = ioutil.WriteFile("data/private_key.der", x509.MarshalPKCS1PrivateKey(key), 0600)
		if err != nil {
			panic(err)
		}
		return key
	}
	if err != nil {
		panic(err)
	}
	key, err := x509.ParsePKCS1PrivateKey(b)
	if err != nil {
		panic(err)
	}
	return key
}

func prefixSuffix(s, prefix, suffix string) (string, bool) {
	if len(s) < len(prefix)+len(suffix) {
		return "", false
	}
	if s[:len(prefix)] != prefix {
		return "", false
	}
	s = s[len(prefix):]
	if s[len(s)-len(suffix):] != suffix {
		return "", false
	}
	return s[:len(s)-len(suffix)], true
}

func RequireSuffix(suffix string, h http.Handler) pub.HandlerFunc {
	return func(_ context.Context, w http.ResponseWriter, r *http.Request) (bool, error) {
		if strings.HasSuffix(r.URL.Path, suffix) {
			h.ServeHTTP(w, r)
			return true, nil
		}

		return false, nil
	}
}

func Wrap(main http.Handler, handlers ...pub.HandlerFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Vary", "Accept")

		in, err := httputil.DumpRequest(r, true)
		if err != nil {
			handleError(w, r, err)
			return
		}

		for _, h := range handlers {
			handled, err := h(r.Context(), w, r)
			if err == errNotFound {
				http.NotFound(w, r)
				return
			}
			if err != nil {
				log.Printf("for incoming request:\n%s", in)
				handleError(w, r, err)
				return
			}
			if handled {
				return
			}
		}

		main.ServeHTTP(w, r)
	})
}

var actor pub.FederatingActor
var fediDB *fediDatabase
var pubKey string
var keyID = "key-2"

func initFederation() []pub.HandlerFunc {
	privKey := initPrivateKey()
	pubKeyBytes, err := x509.MarshalPKIXPublicKey(privKey.Public())
	if err != nil {
		panic(err)
	}
	pubKey = string(pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pubKeyBytes,
	}))

	if _, err := db.Exec(`CREATE TABLE IF NOT EXISTS "federation_remote_objects" (
	"id" TEXT NOT NULL PRIMARY KEY,
	"payload" JSONB NOT NULL
)`); err != nil {
		panic(err)
	}

	if _, err := db.Exec(`CREATE TABLE IF NOT EXISTS "federation_inbox_contents" (
	"inbox" TEXT NOT NULL,
	"id" TEXT NOT NULL,
	PRIMARY KEY("inbox", "id")
)`); err != nil {
		panic(err)
	}

	if _, err := db.Exec(`CREATE TABLE IF NOT EXISTS "possibly_bad_requests" (
	"id" BIGSERIAL NOT NULL PRIMARY KEY,
	"ts" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"payload" TEXT NOT NULL
)`); err != nil {
		panic(err)
	}

	behavior := &fediBehavior{privKey: privKey}
	fediDB = &fediDatabase{privKey: privKey}
	clock := &fediClock{}
	actor = pub.NewFederatingActor(behavior, &fediProtocol{}, fediDB, clock)

	inboxHandler := RequireSuffix("/_inbox", Wrap(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("Cache-Control", "private, max-age=3600")
		w.WriteHeader(http.StatusBadRequest)
		_, _ = io.WriteString(w, "This is the ActivityPub inbox. It only responds to ActivityPub (application/activity+json) requests.\n")
	}), actor.PostInbox, actor.GetInbox))

	outboxHandler := RequireSuffix("/_outbox", Wrap(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("Cache-Control", "private, max-age=3600")
		w.WriteHeader(http.StatusBadRequest)
		_, _ = io.WriteString(w, "This is the ActivityPub outbox. It only responds to ActivityPub (application/activity+json) requests.\n")
	}), actor.PostOutbox, actor.GetOutbox))

	streamsHandler := pub.NewActivityStreamsHandler(fediDB, clock)

	http.HandleFunc("/log/entry/", logEntryHtml)

	http.HandleFunc("/.well-known/host-meta", hostmetaHandler)
	http.HandleFunc("/.well-known/webfinger", webfingerHandler)

	return []pub.HandlerFunc{
		inboxHandler,
		outboxHandler,
		streamsHandler,
	}
}

var _ = registerTemplate("note-page", `
{{template "iconCDN" -}}
{{printf "<https://gw2.lubar.me/log/entry/%d>; rel=alternate; type=application/activity+json" .ID | linkHeader -}}
<article class="log-entry log-entry-{{lower .Type}}">{{.Content}}</article>
{{if or .Prev .Next -}}
<nav class="pagination">
{{if .Prev -}}
<a href="/log/entry/{{.Prev}}" rel="prev">Older</a>
{{end -}}
{{if .Next -}}
<a href="/log/entry/{{.Next}}" rel="next">Newer</a>
{{end -}}
</nav>
{{end -}}
`)

var itemEmojiRegex = regexp.MustCompile(`<span aria-hidden="true">:gw2(item|upgrade)_(\d+):</span>`)
var angleBracketsRegex = regexp.MustCompile(`<[^>]+>`)

func logEntryHtml(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Vary", "Accept")

	if r.URL.Path == "/log/entry/" {
		http.Redirect(w, r, "/log", http.StatusMovedPermanently)
		return
	}

	if !strings.HasPrefix(r.URL.Path, "/log/entry/") || strings.Count(r.URL.Path, "/") != 3 {
		http.NotFound(w, r)
		return
	}

	var logID int
	var published time.Time
	var logType, entries string
	if err := db.QueryRow(`SELECT "id", "type", "time", "entries" FROM "guild_log_combined" WHERE "id" = $1`, r.URL.Path[len("/log/entry/"):]).Scan(&logID, &logType, &published, &entries); err != nil {
		if err != sql.ErrNoRows {
			log.Printf("GET %q: %v", r.URL.Path, err)
		}
		http.NotFound(w, r)
		return
	}
	var prev, next sql.NullString
	if err := db.QueryRow(`SELECT (SELECT "id" FROM "guild_log_combined" WHERE "id" < $1 ORDER BY "id" DESC LIMIT 1) "p", (SELECT "id" FROM "guild_log_combined" WHERE "id" > $2 ORDER BY "id" ASC LIMIT 1) "n"`, logID, logID).Scan(&prev, &next); err != nil {
		handleError(w, r, err)
		return
	}

	obj, err := makeLogObject(logID, logType, published, []byte(entries))
	if err != nil {
		log.Printf("GET %q: %v", r.URL.Path, err)
		http.NotFound(w, r)
		return
	}
	if streams.IsOrExtendsActivityStreamsTombstone(obj) {
		http.NotFound(w, r)
		return
	}

	note := obj.(vocab.ActivityStreamsNote)

	w.Header().Set("Cache-Control", "private, max-age=3600")

	content := note.GetActivityStreamsContent().At(0).GetXMLSchemaString()
	content = itemEmojiRegex.ReplaceAllStringFunc(content, func(text string) string {
		matches := itemEmojiRegex.FindStringSubmatch(text)
		id, err := strconv.Atoi(matches[2])
		if err != nil {
			return ""
		}

		switch matches[1] {
		case "item":
			item, err := getItemInfo(id)
			if err != nil {
				return ""
			}
			s, _ := simpleRenderTemplate("icon-32", item)
			return s
		case "upgrade":
			item, ok := guildUpgrades[id]
			if !ok {
				return ""
			}
			s, _ := simpleRenderTemplate("icon-32", item)
			return s
		default:
			return ""
		}
	})

	renderTemplate(w, r, "note-page", &struct {
		ID      int
		Type    string
		Note    vocab.ActivityStreamsNote
		Prev    string
		Next    string
		Content template.HTML
	}{
		ID:      logID,
		Type:    logType,
		Note:    note,
		Prev:    prev.String,
		Next:    next.String,
		Content: template.HTML(content),
	}, headerData{
		Title:       fullGuildName() + " guild log entry #" + strconv.Itoa(logID),
		Description: angleBracketsRegex.ReplaceAllLiteralString(strings.Replace(content, "</p><p>", "\n\n", -1), ""),
	}, published)
}

var hostmetaBytes = []byte(`<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
<Link rel="lrdd" type="application/xrd+xml" template="https://gw2.lubar.me/.well-known/webfinger?resource={uri}"/>
</XRD>
`)

func hostmetaHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "public, max-age=86400")
	w.Header().Set("Content-Type", "application/xrd+xml; charset=utf-8")

	http.ServeContent(w, r, "", time.Time{}, bytes.NewReader(hostmetaBytes))
}

func webfingerHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "public, max-age=86400")
	resource := r.URL.Query().Get("resource")
	if resource == "" {
		http.Error(w, "The 'resource' query string parameter is required.", http.StatusBadRequest)
		return
	}
	acct, ok := prefixSuffix(resource, "acct:", "@gw2.lubar.me")
	if !ok {
		http.Error(w, "Resource not handled by this server.", http.StatusNotFound)
		return
	}
	acct = strings.ToLower(acct)
	if acct != url.QueryEscape(acct) {
		http.Error(w, "Resource not handled by this server.", http.StatusNotFound)
		return
	}

	acct = strings.Replace(acct, "_", " ", -1)

	if len(acct) > 4 && acct[len(acct)-5] != '.' {
		// add dot back in
		isGW2AccountName := true
		for i := 1; i <= 4; i++ {
			if ch := acct[len(acct)-i]; ch < '0' || ch > '9' {
				isGW2AccountName = false
				break
			}
		}
		if isGW2AccountName {
			acct = acct[:len(acct)-4] + "." + acct[len(acct)-4:]
		}
	}

	switch strings.Count(acct, ".") {
	case 0:
		// ok
	case 1:
		acct = "members/" + acct
	default:
		http.Error(w, "Resource not handled by this server.", http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/jrd+json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"subject": resource,
		"aliases": []interface{}{"https://gw2.lubar.me/" + acct},
		"links": []map[string]interface{}{
			{
				"rel":  "http://webfinger.net/rel/profile-page",
				"type": "text/html",
				"href": "https://gw2.lubar.me/" + acct,
			},
			{
				"rel":  "self",
				"type": "application/activity+json",
				"href": "https://gw2.lubar.me/" + acct,
			},
		},
	})
}

func maybeForceUnfollow(iri string) {
	_, err := db.Exec(`
UPDATE "federation_remote_objects"
   SET "payload" = "payload" || jsonb_build_object('items', ("payload"->'items') - $1)
 WHERE "id" = 'https://gw2.lubar.me/log/_followers'
`, iri)
	log.Println("forced deleted account", iri, "to unfollow log:", err)
}

func isBlocked(iri *url.URL) bool {
	if iri.Host == "pawoo.net" {
		// too many spam follows; LubarGW2 is currently broken and requires manual
		// deletion of follows from deleted accounts. no legitimate follows, so easier
		// to just block the whole domain for now.
		return true
	}

	return false
}

func requireCollection(c vocab.Type, err error, id *url.URL) (vocab.ActivityStreamsCollection, error) {
	if coll, ok := c.(vocab.ActivityStreamsCollection); ok {
		return coll, err
	}

	if err == nil || err == errNotFound {
		coll := streams.NewActivityStreamsCollection()
		idProperty := streams.NewJSONLDIdProperty()
		idProperty.Set(id)
		coll.SetJSONLDId(idProperty)
		coll.SetActivityStreamsItems(streams.NewActivityStreamsItemsProperty())
		return coll, nil
	}

	return nil, err
}

func removeBlocked(coll vocab.ActivityStreamsCollection, err error) (vocab.ActivityStreamsCollection, error) {
	if err != nil {
		return coll, err
	}

	seen := make(map[string]bool)

	items := coll.GetActivityStreamsItems()
	for i := 0; i < items.Len(); i++ {
		u := items.At(i).GetIRI().String()

		if seen[u] || isBlocked(items.At(i).GetIRI()) {
			items.Remove(i)
			i--
		}

		seen[u] = true
	}

	return coll, nil
}

// pub.Clock implementation

type fediClock struct{}

func (*fediClock) Now() time.Time { return time.Now() }
