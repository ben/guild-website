package main

import (
	"net/http"
	"time"
)

func init() {
	http.HandleFunc("/bank", bankHandler)
}

var _ = registerTemplate("bank-page", `
{{$guild := guild -}}
{{range $vault := $guild.Stash -}}
<div class="guild-vault">
<h2>{{template "icon-32" .Upgrade}} {{.Upgrade.Name}}</h2>
<blockquote>{{.Note}}</blockquote>
<div class="main">
<div class="inventory">
{{range $i, $item := .Inventory}}
<div class="item">
{{with $item}}
<label for="item-{{$vault.UpgradeID}}-{{$i}}" aria-labeledby="item-description-{{$vault.UpgradeID}}-{{$i}}">{{template "main-icon-64" .Item}}</label>
<span class="count">{{.Count}}</span>
{{end}}
</div>
{{end}}
<div class="coins">{{.CoinsFmt}}</div>
</div>
<div class="item-viewer">
{{range $i, $item := .Inventory}}
{{with $item}}{{with .Item}}
<input type="radio" name="item-{{$vault.UpgradeID}}" id="item-{{$vault.UpgradeID}}-{{$i}}">
<div class="item" id="item-description-{{$vault.UpgradeID}}-{{$i}}">
<h2>{{template "icon-32" .}} <a href="https://wiki.guildwars2.com/index.php?title=Special:Search&search={{.ChatLink}}">{{.Name}}</a></h2>
<pre class="item-description">{{if .Description}}{{.Description}}

{{end}}{{with .Details}}{{if .Duration}}Applies {{.Duration}} of {{if .Icon}}{{template "icon-16" .}}{{end}}{{.Name}}

{{.Description}}{{end}}{{end}}</pre>
{{end}}
</div>
{{end}}{{end}}
</div>
</div>
</div>
{{end}}
`)

func bankHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "public, max-age=30, stale-while-revalidate=3600")
	renderTemplate(w, r, "bank-page", struct{}{}, headerData{
		Title: fullGuildName() + " Guild Vault",
	}, time.Time{})
}
