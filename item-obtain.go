package main

type itemVendor struct {
	Cost  int
	Count int
}

func (v itemVendor) CoinsFmt() string {
	return formatCoins(int64(v.Cost))
}

var itemsVendor = map[int]itemVendor{
	12156: {80, 10},    // Jug of Water
	13006: {1480, 1},   // Rune of Holding
	13007: {5000, 1},   // Major Rune of Holding
	13008: {20000, 1},  // Greater Rune of Holding
	13009: {100000, 1}, // Superior Rune of Holding
	13010: {496, 1},    // Minor Rune of Holding
	19663: {2504, 1},   // Bottle of Elonian Wine
	19676: {10000, 1},  // Icy Runestone
	21156: {600, 1},    // Arrow Cart Blueprints
	21157: {600, 1},    // Ballista Blueprints
	21158: {1200, 1},   // Catapult Blueprints
	21159: {10000, 1},  // Alpha Siege Golem Blueprints
	21160: {2400, 1},   // Trebuchet Blueprints
	21161: {600, 1},    // Flame Ram Blueprints
	36507: {20000, 1},  // Custom Arena Time Token
	46747: {1496, 10},  // Thermocatalytic Reagent
}

var itemsOther = map[int]string{
	19925: "Cathedral of Glorious Victory", // Obsidian Shard
	20017: "Black Lion Trading Company",    // Trading Post Express
	20799: "Miyani",                        // Mystic Crystal
	41824: "Super Adventure Box",           // Continue Coin
	43766: "Daily Achievements",            // Tome of Knowledge
	46500: "Super Adventure Box",           // World 1 Super Boom Box
	46504: "Super Adventure Box",           // World 2 Super Boom Box
	67826: "The Silverwastes",              // Silverwastes Shovel
	68110: "Daily Achievements",            // Potion of PvP Reward
	69392: "Tangled Depths",                // Ley Line Spark
	69432: "Auric Basin",                   // Pile of Auric Dust
	69434: "Verdant Brink",                 // Bottle of Airship Oil
	71473: "Guild Hall",                    // Badge of Tribute
	78600: "Daily Achievements",            // Potion of WvW Rewards
}
