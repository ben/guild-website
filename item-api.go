package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

var itemCache = struct {
	m map[int][]byte
	sync.Mutex
}{
	m: func() map[int][]byte {
		f, err := os.Open("data-cache/items.json")
		if err != nil {
			panic(err)
		}
		defer f.Close()

		var allItems []json.RawMessage
		if err = json.NewDecoder(f).Decode(&allItems); err != nil {
			panic(err)
		}

		m := make(map[int][]byte)

		for _, raw := range allItems {
			var data struct {
				ID int `json:"id"`
			}
			if err = json.Unmarshal(raw, &data); err != nil {
				panic(err)
			}

			m[data.ID] = raw
		}

		return m
	}(),
}

type guildUpgrade struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Type        string `json:"type"`

	// only if Type is BankBag:
	BagMaxItems int   `json:"bag_max_items,omitempty"`
	BagMaxCoins int64 `json:"bag_max_coins,omitempty"`

	Icon          string             `json:"icon"`
	BuildTime     int                `json:"build_time"`
	RequiredLevel int                `json:"required_level"`
	Experience    int                `json:"experience"`
	Prerequisites []int              `json:"prerequisites"`
	Costs         []guildUpgradeCost `json:"costs"`
	Coin          int                `json:"-"`
	CoinsFmt      string             `json:"-"`
	Aetherium     int                `json:"-"`
	Favor         int                `json:"-"`
	Advisor       advisor            `json:"-"`
}

func (u *guildUpgrade) IsUpgrade() bool {
	return u != nil && !notGuildUpgradeType(u.Type)
}

func (u *guildUpgrade) URL() string {
	name := u.Name

	if u.Type == "Decoration" && len(u.Costs) == 1 && u.Costs[0].Type == "Item" {
		name = u.Costs[0].Name
	}

	return "https://wiki.guildwars2.com/wiki/" + strings.ReplaceAll(name, " ", "_")
}

type guildUpgradeCost struct {
	Type   string `json:"type"`
	Name   string `json:"name"`
	Count  int    `json:"count"`
	ItemID int    `json:"item_id,omitempty"`
}

var forceUpgradeAdvisor = map[int]advisor{
	// expeditions
	410: sarah,
	629: kogga,
	871: hashim,

	// restoration 1
	240: grímur,
	285: rekka,
	288: sophia,
	350: brine,
	359: gamli,
	380: kensho,
	434: castor,

	// restoration 2
	130: sophia,
	162: castor,
	204: kensho,
	424: gamli,
	506: rekka,
	602: brine,

	// misc
	72:  grímur,
	159: gamli,
	208: kensho,
	213: brine,
	232: kensho,
	256: sophia,
	287: rekka,
	301: rekka,
	303: sophia,
	309: brine,
	310: rekka,
	324: rekka,
	379: castor,
	391: rekka,
	397: pall,
	408: kensho,
	417: kensho,
	476: sophia,
	491: sophia,
	550: sophia,
	586: kensho,
	605: sophia,
	765: brine,
	770: kensho,
	773: kensho,
	774: kensho,
}

var maxGuildExp int
var guildUpgrades = func() map[int]*guildUpgrade {
	f, err := os.Open("data-cache/upgrades.json")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var data []*guildUpgrade

	if err = json.NewDecoder(f).Decode(&data); err != nil {
		panic(err)
	}

	upgrades := make(map[int]*guildUpgrade)
	for _, upgrade := range data {
		maxGuildExp += upgrade.Experience
		upgrade.Name = itemNameReplacements.Replace(upgrade.Name)
		upgrade.Description = itemNameReplacements.Replace(upgrade.Description)
		for _, c := range upgrade.Costs {
			if c.Type == "Collectible" && c.Name == "Guild Favor" {
				upgrade.Favor += c.Count
			} else if c.Type == "Currency" && c.Name == "Aetherium" {
				upgrade.Aetherium += c.Count
			} else if c.Type == "Coins" {
				upgrade.Coin += c.Count
				upgrade.CoinsFmt = formatCoins(int64(upgrade.Coin))
			}
		}

		if s, ok := maxSize[upgrade.ID]; ok {
			if _, ok = miningRate[upgrade.ID]; !ok {
				upgrade.Description = fmt.Sprintf("Increase storage capacity for aetherium to %d.", s)
			}
		} else if r, ok := miningRate[upgrade.ID]; ok {
			upgrade.Description = fmt.Sprintf("Produce aetherium at the faster rate of %.1f per minute.", 1/r.Minutes())
		}

		upgrades[upgrade.ID] = upgrade

		if a, ok := forceUpgradeAdvisor[upgrade.ID]; ok {
			upgrade.Advisor = a
		} else if upgrade.IsUpgrade() {
			found := false
			for _, id := range upgrade.Prerequisites {
				if a, ok = forceUpgradeAdvisor[id]; ok {
					upgrade.Advisor = a
					found = true
					break
				}
				if pre, ok := upgrades[id]; ok && pre.Advisor != advisor(0) {
					upgrade.Advisor = a
					found = true
					break
				}
			}
			if !found {
				panic(fmt.Sprintf("can't figure out advisor for %d: %s", upgrade.ID, upgrade.Name))
			}
		}
	}

	return upgrades
}()

func getItemInfoRaw(id int) ([]byte, error) {
	itemCache.Lock()
	defer itemCache.Unlock()

	if b, ok := itemCache.m[id]; ok {
		if b == nil {
			return nil, fmt.Errorf("item not found for ID %d (cached)", id)
		}
		return b, nil
	}

	resp, err := http.Get("https://api.guildwars2.com/v2/items/" + strconv.Itoa(id) + "?v=" + arbitraryAPIVersionDate)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNotFound {
		itemCache.m[id] = nil
		return nil, fmt.Errorf("item not found for ID %d", id)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("getting item %d failed: %q", id, resp.Status)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	itemCache.m[id] = b
	return b, nil
}

type itemInfo struct {
	ID          int      `json:"id"`
	RealName    string   `json:"-"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	ChatLink    string   `json:"chat_link"`
	Icon        string   `json:"icon"`
	Flags       []string `json:"flags"`
	Details     *struct {
		Name        string        `json:"name"`
		DurationMS  int64         `json:"duration_ms"`
		Duration    time.Duration `json:"-"`
		Icon        string        `json:"icon"`
		Description string        `json:"description"`
	} `json:"details,omitempty"`
}

func (i itemInfo) Tradable() bool {
	for _, f := range i.Flags {
		switch f {
		case "AccountBound", "SoulbindOnAcquire":
			return false
		default:
		}
	}
	return true
}

var itemNameReplacements = strings.NewReplacer(
	"Ley-Line Infused Tool", "Mining Fork",
	"Silverwastes Shovel", "Mining Spoon",
	"Tome of Knowledge", "Book That Makes You Smart If You Eat It",
	"<c=@flavor>", "", // temp
	"</c>", "", // temp
)

func getItemInfo(id int) (itemInfo, error) {
	b, err := getItemInfoRaw(id)
	if err != nil {
		return itemInfo{}, err
	}

	return parseItemInfo(b)
}

func parseItemInfo(b []byte) (itemInfo, error) {
	var data itemInfo

	if err := json.Unmarshal(b, &data); err != nil {
		return itemInfo{}, err
	}

	data.RealName = data.Name
	data.Name = itemNameReplacements.Replace(data.Name)
	data.Description = itemNameReplacements.Replace(data.Description)
	if data.Details != nil {
		data.Details.Name = itemNameReplacements.Replace(data.Details.Name)
		data.Details.Description = itemNameReplacements.Replace(data.Details.Description)
		data.Details.Duration = time.Duration(data.Details.DurationMS) * time.Millisecond
	}

	return data, nil
}

var renderURLRegex = regexp.MustCompile(`\Ahttps://render\.guildwars2\.com/file/([^/]+)/([0-9]+)\.png\z`)

func gw2IconSize(size int, renderURL string) (string, error) {
	if size != 16 && size != 32 && size != 64 {
		return "", fmt.Errorf("unsupported icon size for gw2treasures CDN: %d", size)
	}

	match := renderURLRegex.FindStringSubmatch(renderURL)
	if match == nil {
		return "", fmt.Errorf("unexpected format for render.guildwars2.com URL: %q", renderURL)
	}

	_ = size

	return fmt.Sprintf("https://render.guildwars2.com/file/%s/%s.png", match[1], match[2]), nil
}
