package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strings"
	"time"
)

func init() {
	http.HandleFunc("/treasury", treasuryHandler)
}

var _ = registerTemplate("treasury-page", `
<style nonce="{{nonce}}">
{{range $max, $pct := .AetheriumMax -}}
{{if gt $max $.Guild.Info.AetheriumMax -}}
.bad.max-{{$max}} { --pct: {{$pct}}%; }
{{end -}}
{{end -}}
</style>
<section id="guild-level">
<h2>Guild Level</h2>
<div class="big-num">{{$.Guild.Info.Level}}</div>
<progress value="{{.CurrentLevelExperience}}" max="{{.NextLevelExperience}}"></progress><br>
to next<br>
<progress value="{{$.Guild.Info.Experience}}" max="{{maxGuildExp}}"></progress><br>
to goal
</section>
{{with .Aetherium -}}
<section class="treasury-aetherium">
<h2>Aetherium Mining Progress</h2>
<progress id="aetherium-progress" data-current="{{.Current}}"{{if eq .Current .Max}} value="1" max="1"{{end}} data-max="{{.Max}}" data-last="{{.Last.Unix}}000" data-rate="{{.Rate.Seconds | printf "%.0f"}}"></progress>
<div class="aetherium"><progress value="{{.Current}}" max="{{.Max}}"></progress> <span class="current">{{.Current}}</span> of {{.Max}}</div>
<noscript>plus 1 per {{.Rate.Seconds | printf "%.0f"}} seconds.</noscript>
{{template "aetherium.js"}}
</section>
{{end -}}
{{if .Stocks -}}
<section class="treasury-stocks" id="stocks">
<h2>Stocks</h2>
<dl>
{{range .Stocks -}}
<dt><img src="{{gw2ico 16 .Item.Icon}}" srcset="{{gw2ico 32 .Item.Icon}} 2x, {{gw2ico 64 .Item.Icon}} 4x" alt="" class="inline" aria-hidden="true"> <a href="{{.Link}}">{{.Item.Name}}</a></dt>
<dd><progress value="{{.Count}}" max="{{.TotalNeeded}}"></progress> need {{.TotalRemaining}} more
{{if .Vendor -}}
<br>Costs {{.Vendor.CoinsFmt}}{{if ne .Vendor.Count 1}} for {{.Vendor.Count}}{{else}} each{{end}}.
{{end -}}
{{if .Hint -}}
<br>Acquired from <em>{{.Hint}}</em>.
{{end -}}
{{if and .Item.Tradable (not .Vendor) -}}
<br><span class="tp-price" data-item-id="{{.ItemID}}"{{if .Vendor}} data-vendor-price="{{.Vendor.Cost}}" data-vendor-count="{{.Vendor.Count}}"{{end}}>Tradable</span>
{{end -}}
</dd>
{{end -}}
</dl>
{{template "tp-price.js"}}
</section>
{{end -}}
{{range $u := .ByUpgrade -}}
<section class="treasury-upgrade upgrade-advisor-{{$u.Upgrade.Advisor}}" id="upgrade-{{$u.UpgradeID}}">
<h2><img src="{{gw2ico 32 $u.Upgrade.Icon}}" srcset="{{gw2ico 64 $u.Upgrade.Icon}} 2x" alt="" class="inline" aria-hidden="true"> <a href="https://wiki.guildwars2.com/wiki/{{$u.Upgrade.Name}}">{{$u.Upgrade.Name}}</a></h2>
<p>{{$u.Upgrade.Description}}</p>
{{if $u.Unlocks -}}
<ul>
{{range $r := $u.Unlocks -}}
<li>Unlocks upgrade: <img src="{{gw2ico 16 $r.Icon}}" srcset="{{gw2ico 32 $r.Icon}} 2x, {{gw2ico 64 $r.Icon}} 4x" alt="" class="inline" aria-hidden="true"> <a href="#upgrade-{{$r.ID}}">{{$r.Name}}</a></li>
{{end -}}
</ul>
{{end -}}
<p><em>Grants {{$u.Upgrade.Experience}} guild experience.</em></p>
{{if $u.Unmet -}}
<ul>
{{range $r := $u.Unmet -}}
<li>Requires upgrade: <img src="{{gw2ico 16 $r.Icon}}" srcset="{{gw2ico 32 $r.Icon}} 2x, {{gw2ico 64 $r.Icon}} 4x" alt="" class="inline" aria-hidden="true"> <a href="#upgrade-{{$r.ID}}">{{$r.Name}}</a></li>
{{end -}}
</ul>
{{end -}}
<dl>
{{if $u.Upgrade.Coin -}}
<dt><a href="https://wiki.guildwars2.com/wiki/Coin">Coin</a></dt>
{{if gt (len $.Guild.Stash) 0 -}}
<dd><progress value="{{(index $.Guild.Stash 0).Coins}}" max="{{$u.Upgrade.Coin}}"></progress> {{(index $.Guild.Stash 0).CoinsFmt}} of {{$u.Upgrade.CoinsFmt}}</dd>
{{else -}}
<dd><progress value="0" max="{{$u.Upgrade.Coin}}"></progress> 0 of {{$u.Upgrade.CoinsFmt}}</dd>
{{end -}}
{{end -}}
{{if $u.Upgrade.Favor -}}
<dt><a href="https://wiki.guildwars2.com/wiki/Favor">Favor</a></dt>
<dd><progress value="{{$.Guild.Info.Favor}}" max="{{$u.Upgrade.Favor}}"></progress> {{$.Guild.Info.Favor}} of {{$u.Upgrade.Favor}} (max {{$.Guild.Info.FavorMax}})</dd>
{{end -}}
{{if $u.Upgrade.Aetherium -}}
<dt><a href="https://wiki.guildwars2.com/wiki/Aetherium">Aetherium</a></dt>
<dd class="aetherium"><progress value="{{$.Guild.Info.Aetherium}}" max="{{$u.Upgrade.Aetherium}}"{{if gt $u.Upgrade.Aetherium $.Guild.Info.AetheriumMax}} class="bad max-{{$u.Upgrade.Aetherium}}"{{end}}></progress> <span class="current">{{$.Guild.Info.Aetherium}}</span> of {{$u.Upgrade.Aetherium}} (max {{$.Guild.Info.AetheriumMax}})</dd>
{{end -}}
{{range $u.Items}}
<dt><img src="{{gw2ico 16 .Item.Item.Icon}}" srcset="{{gw2ico 32 .Item.Item.Icon}} 2x, {{gw2ico 64 .Item.Item.Icon}} 4x" alt="" class="inline" aria-hidden="true"> <a href="https://wiki.guildwars2.com/index.php?title=Special:Search&search={{.Item.Item.ChatLink}}">{{.Item.Item.Name}}</a></dt>
<dd><progress value="{{.Item.Count}}" max="{{.Needed}}"></progress> {{.Item.Count}} of {{.Needed}} (max {{.Item.TotalNeeded}})</dd>
{{end -}}
</dl>
</section>
{{end -}}
<details>
<summary><h2 id="unavailable">Unavailable</h2></summary>
{{range $u := .Unavailable -}}
<section class="treasury-upgrade treasury-upgrade-unavailable" id="upgrade-{{$u.Upgrade.ID}}">
<h2><img src="{{gw2ico 32 $u.Upgrade.Icon}}" srcset="{{gw2ico 64 $u.Upgrade.Icon}} 2x" alt="" class="inline" aria-hidden="true"> <a href="https://wiki.guildwars2.com/wiki/{{$u.Upgrade.Name}}">{{$u.Upgrade.Name}}</a></h2>
<p>{{$u.Upgrade.Description}}</p>
<ul>
{{with $u.RequiredExp -}}
<li>Requires {{.}} more guild experience</li>
{{end -}}
{{range $r := $u.Unmet -}}
<li>Requires upgrade: <img src="{{gw2ico 16 $r.Icon}}" srcset="{{gw2ico 32 $r.Icon}} 2x, {{gw2ico 64 $r.Icon}} 4x" alt="" class="inline" aria-hidden="true"> <a href="#upgrade-{{$r.ID}}">{{$r.Name}}</a></li>
{{end -}}
</ul>
</section>
{{end -}}
</details>
<h2 id="completed">Completed</h2>
{{range $u := .Completed -}}
<section class="treasury-upgrade treasury-upgrade-completed" id="upgrade-{{$u.Upgrade.ID}}">
<h2><img src="{{gw2ico 32 $u.Upgrade.Icon}}" srcset="{{gw2ico 64 $u.Upgrade.Icon}} 2x" alt="" class="inline" aria-hidden="true"> <a href="https://wiki.guildwars2.com/wiki/{{$u.Upgrade.Name}}">{{$u.Upgrade.Name}}</a></h2>
<p>{{$u.Upgrade.Description}}</p>
<p><em>Unlocked {{$u.Time}}.</em></p>
</section>
{{end -}}
`)

func treasuryHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "public, max-age=30, stale-while-revalidate=3600")

	guild, err := getGuildData()
	if err != nil {
		handleError(w, r, err)
		return
	}

	upgradeTimestamps, err := getUpgradeTimestamps()
	if err != nil {
		handleError(w, r, err)
		return
	}

	attainable, isCompleted := make(map[int]bool), make(map[int]bool)
	completed := make([]*completedUpgrade, len(guild.Upgrades))
	for i, id := range guild.Upgrades {
		completed[i] = &completedUpgrade{
			Upgrade: guildUpgrades[id],
			Time:    upgradeTimestamps[id],
		}
		attainable[id], isCompleted[id] = true, true
	}

	var byUpgrade []*treasuryUpgrade
	byUpgradeMap := make(map[int]*treasuryUpgrade)
	for _, item := range guild.Treasury {
		for _, u := range item.NeededBy {
			data := byUpgradeMap[u.UpgradeID]
			if data == nil {
				attainable[u.UpgradeID] = true
				data = &treasuryUpgrade{
					UpgradeID: u.UpgradeID,
					Upgrade:   u.Upgrade,
					Ready:     true,
				}
				byUpgrade = append(byUpgrade, data)
				byUpgradeMap[u.UpgradeID] = data
			}
			data.Items = append(data.Items, treasuryUpgradeItem{
				Item:   item,
				Needed: u.Count,
			})
			if item.Count < u.Count {
				data.Ready = false
			}
		}
	}

	for _, u := range guildUpgrades {
		if notGuildUpgradeType(u.Type) || attainable[u.ID] {
			continue
		}

		if u.RequiredLevel > guild.Info.Level {
			continue
		}

		allReqs := true
		for _, r := range u.Prerequisites {
			if !isCompleted[r] {
				allReqs = false
			}
		}

		if !allReqs {
			continue
		}

		// Upgrade doesn't require any items (only currency).
		attainable[u.ID] = true
		data := &treasuryUpgrade{
			UpgradeID: u.ID,
			Upgrade:   u,
			Ready:     true,
		}
		byUpgrade = append(byUpgrade, data)
		byUpgradeMap[u.ID] = data
	}

	aetheriumMax := make(map[int]float64)
	for _, u := range byUpgrade {
		if guild.Info.AetheriumMax < u.Upgrade.Aetherium {
			aetheriumMax[u.Upgrade.Aetherium] = 100 * float64(guild.Info.AetheriumMax) / float64(u.Upgrade.Aetherium)
			knownUnmet := make(map[int]bool)
			for _, id := range maxSizeUpgrades {
				if maxSize[id] > u.Upgrade.Aetherium {
					break
				}

				u.Unmet = addUnmet(u.Unmet, guildUpgrades[id], knownUnmet, isCompleted, attainable)
			}
		}
	}

	var unavailable []*unavailableUpgrade
	for _, u := range guildUpgrades {
		if notGuildUpgradeType(u.Type) || attainable[u.ID] {
			continue
		}

		exp := u.RequiredLevel*100 - guild.Info.Experience
		if exp < 0 {
			exp = 0
		}
		knownUnmet := make(map[int]bool)
		var unmet []*guildUpgrade
		for _, id := range u.Prerequisites {
			unmet = addUnmet(unmet, guildUpgrades[id], knownUnmet, isCompleted, attainable)
		}

		if guild.Info.AetheriumMax < u.Aetherium {
			for _, id := range maxSizeUpgrades {
				if maxSize[id] > u.Aetherium {
					break
				}

				unmet = addUnmet(unmet, guildUpgrades[id], knownUnmet, isCompleted, attainable)
			}
		}

		unavailable = append(unavailable, &unavailableUpgrade{
			Upgrade:     u,
			RequiredExp: exp,
			Unmet:       unmet,
		})
	}

	sort.Sort(currentUpgrades(byUpgrade))
	sort.Sort(completedUpgrades(completed))
	sort.Sort(unavailableUpgrades(unavailable))

	for _, u := range unavailable {
		if u.RequiredExp <= 0 && len(u.Unmet) == 1 {
			if t, ok := byUpgradeMap[u.Unmet[0].ID]; ok {
				t.Unlocks = append(t.Unlocks, u.Upgrade)
			}
		}
	}

	for _, c := range completed {
		c.Time = c.Time.Truncate(time.Second)
	}

	nextLevelExperience := 100
	if guild.Info.Level*100+100 > maxGuildExp {
		nextLevelExperience = maxGuildExp % 100
	}

	type stocksItem struct {
		*guildTreasuryItem
		Link   string
		Vendor *itemVendor
		Hint   string
	}
	var stocks []stocksItem
	for _, t := range guild.Treasury {
		if t.TotalNeeded > t.Count {
			var s = stocksItem{
				guildTreasuryItem: t,
				Link:              "https://wiki.guildwars2.com/wiki/" + strings.ReplaceAll(t.Item.RealName, " ", "_") + "#Acquisition",
			}

			if v, ok := itemsVendor[t.ItemID]; ok {
				s.Vendor = &v
			} else if h, ok := itemsOther[t.ItemID]; ok {
				s.Hint = h
			} else {
				s.Link = fmt.Sprintf("https://gw2efficiency.com/crafting/calculator/a~0!b~1!c~0!d~%d-%d", t.TotalRemaining(), t.ItemID)
			}

			stocks = append(stocks, s)
		}
	}

	sort.SliceStable(stocks, func(i, j int) bool {
		if stocks[i].Vendor != nil && stocks[j].Vendor == nil {
			return true
		}
		if stocks[i].Vendor == nil && stocks[j].Vendor != nil {
			return false
		}

		if stocks[i].Hint != "" && stocks[j].Hint == "" {
			return true
		}
		if stocks[i].Hint == "" && stocks[j].Hint != "" {
			return false
		}

		return false
	})

	renderTemplate(w, r, "treasury-page", &struct {
		Aetherium    aetheriumState
		Guild        *guildData
		Upgrades     map[int]*guildUpgrade
		Completed    []*completedUpgrade
		ByUpgrade    []*treasuryUpgrade
		Stocks       []stocksItem
		AetheriumMax map[int]float64
		Unavailable  []*unavailableUpgrade

		CurrentLevelExperience int
		NextLevelExperience    int
	}{
		Aetherium:    getAetheriumState(),
		Guild:        guild,
		Upgrades:     guildUpgrades,
		Completed:    completed,
		ByUpgrade:    byUpgrade,
		Stocks:       stocks,
		AetheriumMax: aetheriumMax,
		Unavailable:  unavailable,

		CurrentLevelExperience: guild.Info.Experience % 100,
		NextLevelExperience:    nextLevelExperience,
	}, headerData{
		Title: fullGuildName() + " Guild Treasury",
	}, time.Time{})
}

func notGuildUpgradeType(t string) bool {
	return t == "Decoration" || t == "Consumable" || t == "Claimable" || t == "GuildHall" || t == "GuildHallExpedition"
}

type completedUpgrade struct {
	Upgrade *guildUpgrade
	Time    time.Time
}

type treasuryUpgradeItem struct {
	Item   *guildTreasuryItem
	Needed int
}

type treasuryUpgrade struct {
	UpgradeID int
	Upgrade   *guildUpgrade
	Items     []treasuryUpgradeItem
	Unmet     []*guildUpgrade
	Unlocks   []*guildUpgrade
	Ready     bool
}

type unavailableUpgrade struct {
	Upgrade     *guildUpgrade
	RequiredExp int
	Unmet       []*guildUpgrade
}

func addUnmet(unmet []*guildUpgrade, u *guildUpgrade, knownUnmet, isCompleted, attainable map[int]bool) []*guildUpgrade {
	if knownUnmet[u.ID] || isCompleted[u.ID] {
		return unmet
	}
	knownUnmet[u.ID] = true
	if !attainable[u.ID] {
		for _, id := range u.Prerequisites {
			unmet = addUnmet(unmet, guildUpgrades[id], knownUnmet, isCompleted, attainable)
		}
	}
	return append(unmet, u)
}

func getUpgradeTimestamps() (map[int]time.Time, error) {
	timestamps := make(map[int]time.Time)

	// hardcoded timestamps that were lost to history
	timestamps[240] = time.Date(2021, time.April, 7, 4, 22, 23, 0, time.UTC)
	timestamps[72] = time.Date(2021, time.April, 7, 4, 22, 23, 1, time.UTC)
	timestamps[407] = time.Date(2021, time.April, 12, 17, 0, 2, 0, time.UTC)

	rows, err := db.Query(`SELECT "payload" FROM "guild_log_entries" WHERE "type" = 'upgrade'`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var payload []byte
		if err := rows.Scan(&payload); err != nil {
			return nil, err
		}

		var data struct {
			Time      time.Time `json:"time"`
			UpgradeID int       `json:"upgrade_id"`
		}
		if err := json.Unmarshal(payload, &data); err != nil {
			return nil, err
		}

		timestamps[data.UpgradeID] = data.Time
	}

	return timestamps, rows.Err()
}

type currentUpgrades []*treasuryUpgrade

func (u currentUpgrades) Len() int      { return len(u) }
func (u currentUpgrades) Swap(i, j int) { u[i], u[j] = u[j], u[i] }
func (u currentUpgrades) Less(i, j int) bool {
	if u[i].Upgrade.Type != u[j].Upgrade.Type {
		if u[i].Upgrade.Type == "AccumulatingCurrency" || u[j].Upgrade.Type == "AccumulatingCurrency" {
			return u[i].Upgrade.Type == "AccumulatingCurrency"
		}
	}

	if u[i].Upgrade.Experience != u[j].Upgrade.Experience {
		return u[i].Upgrade.Experience > u[j].Upgrade.Experience
	}

	return u[i].UpgradeID < u[j].UpgradeID
}

type completedUpgrades []*completedUpgrade

func (u completedUpgrades) Len() int           { return len(u) }
func (u completedUpgrades) Swap(i, j int)      { u[i], u[j] = u[j], u[i] }
func (u completedUpgrades) Less(i, j int) bool { return u[i].Time.Before(u[j].Time) }

type unavailableUpgrades []*unavailableUpgrade

func (u unavailableUpgrades) Len() int      { return len(u) }
func (u unavailableUpgrades) Swap(i, j int) { u[i], u[j] = u[j], u[i] }
func (u unavailableUpgrades) Less(i, j int) bool {
	if u[i].Upgrade.RequiredLevel != u[j].Upgrade.RequiredLevel {
		return u[i].Upgrade.RequiredLevel < u[j].Upgrade.RequiredLevel
	}

	return u[i].Upgrade.ID < u[j].Upgrade.ID
}
