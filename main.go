package main

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"
)

var leaderAPIKey = os.Getenv("GUILD_LEADER_API_KEY")
var guildID = os.Getenv("GUILD_ID")

const arbitraryAPIVersionDate = "2019-05-03"

var db *sql.DB

func main() {
	dsn := os.Getenv("DATA_SOURCE_NAME")
	if dsn == "" {
		fmt.Println("DATA_SOURCE_NAME environment variable is required.")
		return
	}

	var err error
	db, err = sql.Open("pgx", dsn)
	if err != nil {
		panic(err)
	}

	go watchGuildLog()

	initStatic()

	wrappers := initFederation()

	wrapped := Wrap(http.DefaultServeMux, wrappers...)

	go backfillDeliveries(context.Background())

	wrappedHandler := func(w http.ResponseWriter, r *http.Request) {
		nonce, err := getNonce()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), nonceKey{}, nonce))
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Max-Age", "86400")
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("Referrer-Policy", "no-referrer-when-downgrade")
		w.Header().Set("Content-Security-Policy", "default-src 'none'; script-src 'self' 'unsafe-inline' 'nonce-"+nonce+"'; style-src 'self' 'unsafe-inline' 'nonce-"+nonce+"'; img-src 'self' https://render.guildwars2.com; connect-src https://api.guildwars2.com; frame-ancestors *; upgrade-insecure-requests; block-all-mixed-content; require-sri-for script style")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		wrapped.ServeHTTP(w, r)
	}

	panic(http.ListenAndServe(":42069", http.HandlerFunc(wrappedHandler)))
}

func getNonce() (string, error) {
	b := make([]byte, 8)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}

	return hex.EncodeToString(b), nil
}

type nonceKey struct{}

type backfillEntry struct {
	ID      int
	Type    string
	Time    time.Time
	Entries []byte
}

func getBackfillEntries(ctx context.Context) ([]backfillEntry, error) {
	rows, err := db.QueryContext(ctx, `SELECT "id", "type", "time", "entries" FROM "guild_log_combined" WHERE "time" >= $1`, time.Now().AddDate(0, -1, 0))
	if err != nil {
		return nil, fmt.Errorf("backfilling failed on initial query: %w", err)
	}
	defer rows.Close()

	var entries []backfillEntry
	for rows.Next() {
		var entry backfillEntry
		if err := rows.Scan(&entry.ID, &entry.Type, &entry.Time, &entry.Entries); err != nil {
			return nil, fmt.Errorf("backfilling failed on initial query (scan): %w", err)
		}
		entries = append(entries, entry)
	}

	return entries, rows.Err()
}

func backfillDeliveries(ctx context.Context) {
	// we don't currently track success of deliveries. just re-deliver everything from the
	// last month. instances already handle duplicate deliveries gracefully.

	entries, err := getBackfillEntries(ctx)
	if err != nil {
		log.Println("[backfill]", err)
		return
	}

	log.Println("[backfill] re-delivering", len(entries), "log entries just to be sure. waiting a minute before each...")

	for _, e := range entries {
		time.Sleep(time.Minute)
		distributeLogEntries([]int{e.ID})
	}
}
