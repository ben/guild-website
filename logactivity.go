package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/streams/vocab"
	"github.com/jackc/pgtype"
)

// Character names that are scrubbed from the log (deadnames, etc.)
var guildLogBlacklist = func() map[string]bool {
	blacklist := map[string]bool{"": true}

	// no guild members if we're just testing templates
	if os.Getenv("DATA_SOURCE_NAME") == "" {
		return blacklist
	}

	f, err := os.Open("data/name_blacklist.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	s := bufio.NewScanner(f)

	for s.Scan() {
		blacklist[s.Text()] = true
	}

	if err = s.Err(); err != nil {
		panic(err)
	}

	return blacklist
}()

func makeLogCreateActivity(id int, logType string, published time.Time, entries []byte) vocab.ActivityStreamsCreate {
	create := streams.NewActivityStreamsCreate()

	idProperty := streams.NewJSONLDIdProperty()
	idProperty.Set(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log/entry/" + strconv.Itoa(id) + "/_create",
	})
	create.SetJSONLDId(idProperty)

	actorProperty := streams.NewActivityStreamsActorProperty()
	actorProperty.AppendIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log",
	})

	attributedToProperty := streams.NewActivityStreamsAttributedToProperty()
	attributedToProperty.AppendIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log",
	})
	create.SetActivityStreamsAttributedTo(attributedToProperty)

	publishedProperty := streams.NewActivityStreamsPublishedProperty()
	publishedProperty.Set(published)
	create.SetActivityStreamsPublished(publishedProperty)

	objectProperty := streams.NewActivityStreamsObjectProperty()
	obj, err := makeLogObject(id, logType, published, entries)
	if err == nil {
		_ = objectProperty.AppendType(obj)
	} else {
		objectProperty.AppendIRI(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log/entry/" + strconv.Itoa(id),
		})
	}
	create.SetActivityStreamsObject(objectProperty)

	toProperty := streams.NewActivityStreamsToProperty()
	toProperty.AppendIRI(&url.URL{
		Scheme:   "https",
		Host:     "www.w3.org",
		Path:     "/ns/activitystreams",
		Fragment: "Public",
	})
	create.SetActivityStreamsTo(toProperty)

	ccProperty := streams.NewActivityStreamsCcProperty()
	ccProperty.AppendIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log/_followers",
	})
	create.SetActivityStreamsCc(ccProperty)

	return create
}

var logTmpl = func() *template.Template {
	t := template.New("").Funcs(template.FuncMap{
		"lower":         strings.ToLower,
		"fullGuildName": fullGuildName,
	})
	template.Must(t.New("mention").Parse(`<a href="https://gw2.lubar.me/members/{{lower .}}" class="mention">{{if eq . "Nightgunner.2896"}}Blergo{{else if eq . "NightgunnerNA.4698"}}Fake Blergo{{else}}{{.}}{{end}}</a>`))
	template.Must(t.New("joined").Parse(`<p>{{template "mention" .User}} has joined {{fullGuildName}}! That’s gay.</p>`))
	template.Must(t.New("stash").Parse(`<p>{{template "mention" .User}} {{.Action}} {{if .Item}}{{.Count}}×{{.Item}}{{else}}{{.CoinsFmt}}{{end}} {{.Direction}} the <a href="https://gw2.lubar.me/bank" rel="tag">guild vault</a>.</p>`))
	template.Must(t.New("rank_change").Parse(`<p>{{if and (eq .OldRank "none") (eq .NewRank "leader")}}{{template "mention" .User}} founded {{fullGuildName}}! That’s gay.{{else}}{{template "mention" .ChangedBy}} changed {{template "mention" .User}}’s rank from <code>{{.OldRank}}</code> to <code>{{.NewRank}}</code>.{{end}}</p>`))
	template.Must(t.New("invited").Parse(`<p>{{template "mention" .InvitedBy}} sent {{template "mention" .User}} an invitation to join the guild.</p>`))
	template.Must(t.New("kick").Parse(`<p>{{if eq .User .KickedBy}}{{template "mention" .User}} left {{fullGuildName}}.{{else}}{{template "mention" .KickedBy}} kicked {{template "mention" .User}} from the guild.{{end}}</p>`))
	template.Must(t.New("invite_declined").Parse(`<p>{{if eq .User .DeclinedBy}}{{template "mention" .User}} declined the invitation to join {{fullGuildName}}.{{else}}{{template "mention" .DeclinedBy}} retracted {{template "mention" .User}}’s invitation to join the guild.{{end}}</p>`))
	template.Must(t.New("upgrade").Parse(`{{if and (eq .Count 0) (eq .Action "queued") .Upgrade.IsUpgrade}}<p>{{fullGuildName}} completed construction of <a href="{{.Upgrade.URL}}" rel="tag"><span aria-hidden="true">:gw2upgrade_{{.Upgrade.ID}}:</span> {{.Upgrade.Name}}</a>!</p><p><em>{{.Upgrade.Description}}</em></p>{{else}}<p>{{template "mention" .User}}{{if and (eq .Count 1) (eq .Action "completed") .Upgrade (not .Upgrade.IsUpgrade)}} stored {{else}} {{.Action}} {{if .Count}}{{.Count}}×{{end}}{{end}}{{if .Upgrade}}<a href="{{.Upgrade.URL}}" rel="tag"><span aria-hidden="true">:gw2upgrade_{{.Upgrade.ID}}:</span> {{.Upgrade.Name}}</a>{{else}}{{.Item}}{{end}}{{if and (eq .Count 1) (eq .Action "completed") .Upgrade (not .Upgrade.IsUpgrade)}} {{lower .Upgrade.Type}}{{end}}.</p>{{end}}`))
	template.Must(t.New("treasury").Parse(`<p>{{template "mention" .User}} donated {{.Count}}×{{.Item}} to the <a href="https://gw2.lubar.me/treasury" rel="tag">guild treasury</a>.</p>`))
	template.Must(t.New("motd").Parse(`<p>{{template "mention" .User}} set a new message of the day:</p><blockquote>{{.MotdFormatted}}</blockquote>`))
	return t
}()

var multiNewline = regexp.MustCompile(`\n{2,}`)

func makeLogObject(id int, logType string, published time.Time, entries []byte) (vocab.Type, error) {
	var buf bytes.Buffer
	var users []string
	var hashtags []string
	var tags []vocab.Type

	addUser := func(id int, user string) {
		users = append(users, user)

		if guildLogBlacklist[user] {
			log.Println("[blacklist] blacklisting entry", id, "due to user", user)

			// avoid this entry showing up again
			if _, err := db.Exec(`INSERT INTO "guild_log_blacklist" ("id") VALUES ($1) ON CONFLICT DO NOTHING`, id); err != nil {
				log.Println("[blacklist] blacklisting entry", id, "failed:", err)
				return
			}

			if _, err := db.Exec(`REFRESH MATERIALIZED VIEW CONCURRENTLY "guild_log_combined"`); err != nil {
				log.Println("[blacklist] refreshing failed", err)
				return
			}
		}
	}

	switch logType {
	case "joined":
		var data [1]struct {
			ID   int       `json:"id"`
			Time time.Time `json:"time"`
			Type string    `json:"type"`
			User string    `json:"user"`
		}

		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		addUser(data[0].ID, data[0].User)
		hashtags = append(hashtags, "GuildWars2")

		if err = logTmpl.ExecuteTemplate(&buf, "joined", &data[0]); err != nil {
			return nil, err
		}
	case "influence":
		return makeLogTombstone(id)
	case "stash":
		var data []struct {
			ID        int           `json:"id"`
			Time      time.Time     `json:"time"`
			Type      string        `json:"type"`
			User      string        `json:"user"`
			Operation string        `json:"operation"`
			ItemID    int           `json:"item_id"`
			Count     int           `json:"count"`
			Coins     int64         `json:"coins"`
			CoinsFmt  string        `json:"-"`
			Action    string        `json:"-"`
			Direction string        `json:"-"`
			Item      template.HTML `json:"-"`
		}
		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		addUser(data[0].ID, data[0].User)
		seenItems := make(map[int]bool)

		for i := range data {
			data[i].CoinsFmt = formatCoins(data[i].Coins)

			switch data[i].Operation {
			case "deposit":
				data[i].Action = "deposited"
				data[i].Direction = "into"
			case "withdraw":
				data[i].Action = "withdrew"
				data[i].Direction = "from"
			case "move":
				// ???
				data[i].Action = "moved"
				data[i].Direction = "within"
			default:
				return nil, fmt.Errorf("unhandled stash operation type: %q", data[i].Operation)
			}

			if data[i].ItemID != 0 {
				var info itemInfo
				data[i].Item, info, err = getItemLink(data[i].ItemID)
				if err != nil {
					return nil, err
				}

				if !seenItems[info.ID] {
					seenItems[info.ID] = true
					emojiObject, err := makeEmoji("item", info.Icon, info.ID, info.Name)
					if err != nil {
						return nil, err
					}
					tags = append(tags, emojiObject)
				}
			}

			if err = logTmpl.ExecuteTemplate(&buf, "stash", &data[i]); err != nil {
				return nil, err
			}
		}
	case "rank_change":
		var data [1]struct {
			ID        int       `json:"id"`
			Time      time.Time `json:"time"`
			Type      string    `json:"type"`
			User      string    `json:"user"`
			ChangedBy string    `json:"changed_by,omitempty"`
			OldRank   string    `json:"old_rank"`
			NewRank   string    `json:"new_rank"`
		}

		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		if data[0].ChangedBy != "" || data[0].OldRank != "none" {
			addUser(data[0].ID, data[0].ChangedBy)
		}
		addUser(data[0].ID, data[0].User)

		if err = logTmpl.ExecuteTemplate(&buf, "rank_change", &data[0]); err != nil {
			return nil, err
		}
	case "kick":
		var data [1]struct {
			ID       int       `json:"id"`
			Time     time.Time `json:"time"`
			Type     string    `json:"type"`
			User     string    `json:"user"`
			KickedBy string    `json:"kicked_by"`
		}

		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		if data[0].User != data[0].KickedBy {
			addUser(data[0].ID, data[0].KickedBy)
		}
		addUser(data[0].ID, data[0].User)

		if err = logTmpl.ExecuteTemplate(&buf, "kick", &data[0]); err != nil {
			return nil, err
		}
	case "invite_declined":
		var data [1]struct {
			ID         int       `json:"id"`
			Time       time.Time `json:"time"`
			Type       string    `json:"type"`
			User       string    `json:"user"`
			DeclinedBy string    `json:"declined_by"`
		}

		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		if data[0].User != data[0].DeclinedBy {
			addUser(data[0].ID, data[0].DeclinedBy)
		}
		addUser(data[0].ID, data[0].User)

		if err = logTmpl.ExecuteTemplate(&buf, "invite_declined", &data[0]); err != nil {
			return nil, err
		}
	case "invited":
		var data [1]struct {
			ID        int       `json:"id"`
			Time      time.Time `json:"time"`
			Type      string    `json:"type"`
			User      string    `json:"user"`
			InvitedBy string    `json:"invited_by"`
		}

		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		addUser(data[0].ID, data[0].InvitedBy)
		addUser(data[0].ID, data[0].User)

		if err = logTmpl.ExecuteTemplate(&buf, "invited", &data[0]); err != nil {
			return nil, err
		}
	case "upgrade":
		var data [1]struct {
			ID        int           `json:"id"`
			Time      time.Time     `json:"time"`
			Type      string        `json:"type"`
			User      string        `json:"user"`
			Action    string        `json:"action"`
			Count     int           `json:"count,omitempty"`
			UpgradeID int           `json:"upgrade_id"`
			RecipeID  int           `json:"recipe_id,omitempty"`
			Upgrade   *guildUpgrade `json:"-"`
		}
		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		if data[0].User != "" {
			addUser(data[0].ID, data[0].User)
		}

		var ok bool
		data[0].Upgrade, ok = guildUpgrades[data[0].UpgradeID]
		if !ok {
			return nil, errNotFound
		}

		emojiObject, err := getUpgradeEmoji(data[0].UpgradeID)
		if err != nil {
			return nil, err
		}
		tags = append(tags, emojiObject)

		if data[0].Count == 0 && data[0].Action == "queued" && data[0].Upgrade.IsUpgrade() {
			hashtags = append(hashtags, "GuildWars2")
		}

		if err = logTmpl.ExecuteTemplate(&buf, "upgrade", &data[0]); err != nil {
			return nil, err
		}
	case "donation":
		var data []struct {
			ID        int           `json:"id"`
			Time      time.Time     `json:"time"`
			Type      string        `json:"type"`
			User      string        `json:"user"`
			ItemID    int           `json:"item_id"`
			Count     int           `json:"count"`
			Item      template.HTML `json:"-"`
			ItemInfo  itemInfo      `json:"-"`
			Action    string        `json:"action"`
			UpgradeID int           `json:"upgrade_id"`
			RecipeID  int           `json:"recipe_id"`
			Upgrade   *guildUpgrade `json:"-"`
		}
		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}
		addUser(data[0].ID, data[0].User)

		var somewood, nice, cloudstorage bool
		seenItems := make(map[int]bool)
		seenUpgrades := make(map[int]bool)
		for i := range data {
			d := &data[i]
			if d.ItemID != 0 {
				d.Item, d.ItemInfo, err = getItemLink(d.ItemID)
				if err != nil {
					return nil, err
				}

				if !seenItems[d.ItemID] {
					seenItems[d.ItemID] = true
					emojiObject, err := getItemEmoji(d.ItemID)
					if err != nil {
						return nil, err
					}
					tags = append(tags, emojiObject)
				}
			}

			d.Upgrade = guildUpgrades[d.UpgradeID]
			if d.Upgrade != nil && !seenUpgrades[d.UpgradeID] {
				seenUpgrades[d.UpgradeID] = true
				emojiObject, err := getUpgradeEmoji(d.UpgradeID)
				if err != nil {
					return nil, err
				}
				tags = append(tags, emojiObject)
			}

			desc := strings.ToLower(d.ItemInfo.Description)
			if strings.Contains(desc, "logs") || strings.Contains(desc, "planks") {
				somewood = true
			}
			if d.Count == 69 {
				nice = true
			}
			if d.ItemInfo.Name == "Super Cloud" {
				cloudstorage = true
			}

			if err = logTmpl.ExecuteTemplate(&buf, d.Type, d); err != nil {
				return nil, err
			}
		}

		if somewood {
			hashtags = append(hashtags, "somewood")
		}
		if nice {
			hashtags = append(hashtags, "nice")
		}
		if cloudstorage {
			hashtags = append(hashtags, "CloudStorage")
		}
	case "motd":
		var data [1]struct {
			ID            int           `json:"id"`
			Time          time.Time     `json:"time"`
			Type          string        `json:"type"`
			User          string        `json:"user"`
			Motd          string        `json:"motd"`
			MotdFormatted template.HTML `json:"-"`
		}

		err := json.Unmarshal(entries, &data)
		if err != nil {
			return nil, err
		}

		addUser(data[0].ID, data[0].User)

		formatted := template.HTMLEscapeString(data[0].Motd)
		formatted = "<p>" + multiNewline.ReplaceAllString(formatted, "</p><p>") + "</p>"
		formatted = strings.Replace(formatted, "\n", "<br>", -1)
		data[0].MotdFormatted = template.HTML(formatted)

		if err = logTmpl.ExecuteTemplate(&buf, "motd", &data[0]); err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("unhandled log type: %q", logType)
	}

	note := streams.NewActivityStreamsNote()

	idProperty := streams.NewJSONLDIdProperty()
	idProperty.Set(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log/entry/" + strconv.Itoa(id),
	})
	note.SetJSONLDId(idProperty)

	attributedToProperty := streams.NewActivityStreamsAttributedToProperty()
	attributedToProperty.AppendIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log",
	})
	note.SetActivityStreamsAttributedTo(attributedToProperty)

	publishedProperty := streams.NewActivityStreamsPublishedProperty()
	publishedProperty.Set(published)
	note.SetActivityStreamsPublished(publishedProperty)

	toProperty := streams.NewActivityStreamsToProperty()
	toProperty.AppendIRI(&url.URL{
		Scheme:   "https",
		Host:     "www.w3.org",
		Path:     "/ns/activitystreams",
		Fragment: "Public",
	})
	note.SetActivityStreamsTo(toProperty)

	ccProperty := streams.NewActivityStreamsCcProperty()
	ccProperty.AppendIRI(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log/_followers",
	})
	note.SetActivityStreamsCc(ccProperty)

	mediaTypeProperty := streams.NewActivityStreamsMediaTypeProperty()
	mediaTypeProperty.Set("text/html")
	note.SetActivityStreamsMediaType(mediaTypeProperty)

	tagProperty := streams.NewActivityStreamsTagProperty()

	for _, user := range users {
		if guildLogBlacklist[user] {
			return makeLogTombstone(id)
		}

		tagMention := streams.NewActivityStreamsMention()
		mentionName := streams.NewActivityStreamsNameProperty()
		mentionName.AppendXMLSchemaString(user)
		tagMention.SetActivityStreamsName(mentionName)
		mentionHref := streams.NewActivityStreamsHrefProperty()
		mentionHref.Set(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/members/" + strings.ToLower(user),
		})
		tagMention.SetActivityStreamsHref(mentionHref)
		tagProperty.AppendActivityStreamsMention(tagMention)
	}

	if len(hashtags) != 0 {
		buf.WriteString("<p>")
		for i, hashtag := range hashtags {
			if i != 0 {
				buf.WriteString(" ")
			}
			buf.WriteString("<a href=\"https://gw2.lubar.me/tag/")
			buf.WriteString(strings.ToLower(hashtag))
			buf.WriteString("\" rel=\"tag\" class=\"mention hashtag\">#<span>")
			buf.WriteString(hashtag)
			buf.WriteString("</span></a>")
			hashtagObject := streams.NewActivityStreamsObject()
			hashtagType := streams.NewJSONLDTypeProperty()
			hashtagType.AppendXMLSchemaString("Hashtag")
			hashtagObject.SetJSONLDType(hashtagType)
			hashtagName := streams.NewActivityStreamsNameProperty()
			hashtagName.AppendXMLSchemaString("#" + strings.ToLower(hashtag))
			hashtagObject.SetActivityStreamsName(hashtagName)
			hashtagID := streams.NewJSONLDIdProperty()
			hashtagID.Set(&url.URL{
				Scheme: "https",
				Host:   "gw2.lubar.me",
				Path:   "/tag/" + strings.ToLower(hashtag),
			})
			hashtagObject.SetJSONLDId(hashtagID)
			tags = append(tags, hashtagObject)
		}
		buf.WriteString("</p>")
	}

	contentProperty := streams.NewActivityStreamsContentProperty()
	contentProperty.AppendXMLSchemaString(buf.String())
	note.SetActivityStreamsContent(contentProperty)

	for _, tag := range tags {
		_ = tagProperty.AppendType(tag)
	}

	note.SetActivityStreamsTag(tagProperty)

	return note, nil
}

func makeLogTombstone(id int) (vocab.ActivityStreamsTombstone, error) {
	tombstone := streams.NewActivityStreamsTombstone()
	idProperty := streams.NewJSONLDIdProperty()
	idProperty.Set(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log/entry/" + strconv.Itoa(id),
	})
	tombstone.SetJSONLDId(idProperty)
	formerType := streams.NewActivityStreamsFormerTypeProperty()
	formerType.AppendXMLSchemaString("Note")
	tombstone.SetActivityStreamsFormerType(formerType)
	return tombstone, nil
}

func distributeLogEntry(id int, logType string, published time.Time, payload []byte) {
	create := makeLogCreateActivity(id, logType, published, payload)
	if create.GetActivityStreamsObject().At(0).IsActivityStreamsTombstone() {
		log.Println("not distributing tombstone log entry", id)
		return
	}
	if create.GetActivityStreamsObject().At(0).IsIRI() {
		log.Println("cannot distribute", logType, "log entry yet", id)
		return
	}

	_, err := actor.Send(context.Background(), &url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log/_outbox",
	}, create)
	if err != nil {
		log.Println("error distributing log entry", id, err)
		return
	}
	log.Println("distributed log entry", id)
}

func formatCoins(copper int64) string {
	var buf []byte
	if copper >= 10000 {
		gold := copper / 10000
		copper %= 10000
		buf = append(buf, ' ')
		buf = strconv.AppendInt(buf, gold, 10)
		buf = append(buf, " gold"...)
	}
	if copper >= 100 {
		silver := copper / 100
		copper %= 100
		buf = append(buf, ' ')
		buf = strconv.AppendInt(buf, silver, 10)
		buf = append(buf, " silver"...)
	}
	if copper >= 1 {
		buf = append(buf, ' ')
		buf = strconv.AppendInt(buf, copper, 10)
		buf = append(buf, " copper"...)
	}

	if len(buf) == 0 {
		return "0 copper"
	}
	return string(buf[1:])
}

func distributeLogEntries(ids []int) {
	var idsArray pgtype.Int4Array
	_ = idsArray.Set(ids)

	rows, err := db.Query(`SELECT "id", "type", "time", "user", "entries" FROM "guild_log_combined" WHERE "id" = ANY($1::integer[])`, &idsArray)
	if err != nil {
		log.Println("error getting log entries for distribution:", err)
		return
	}
	defer rows.Close()

	toDelete := make(map[int]int)

	for rows.Next() {
		var c struct {
			ID      int
			Type    string
			Time    time.Time
			User    string
			Entries string
		}
		if err = rows.Scan(&c.ID, &c.Type, &c.Time, &c.User, &c.Entries); err != nil {
			log.Println("error reading log entries for distribution:", err)
			return
		}

		var entryIDs []struct {
			ID int `json:"id"`
		}
		if err = json.Unmarshal([]byte(c.Entries), &entryIDs); err != nil {
			log.Println("error decoding log entries for distribution:", err)
			return
		}

		for _, e := range entryIDs {
			toDelete[e.ID] = c.ID
		}

		distributeLogEntry(c.ID, c.Type, c.Time, []byte(c.Entries))
	}

	for _, id := range ids {
		delete(toDelete, id)
	}

	for id, newID := range toDelete {
		del := streams.NewActivityStreamsDelete()
		idProperty := streams.NewJSONLDIdProperty()
		idProperty.Set(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log/entry/" + strconv.Itoa(id) + "/_delete",
		})
		del.SetJSONLDId(idProperty)

		toProperty := streams.NewActivityStreamsToProperty()
		toProperty.AppendIRI(&url.URL{
			Scheme:   "https",
			Host:     "www.w3.org",
			Path:     "/ns/activitystreams",
			Fragment: "Public",
		})
		del.SetActivityStreamsTo(toProperty)

		ccProperty := streams.NewActivityStreamsCcProperty()
		ccProperty.AppendIRI(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log/_followers",
		})
		del.SetActivityStreamsCc(ccProperty)

		actorProperty := streams.NewActivityStreamsActorProperty()
		actorProperty.AppendIRI(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log",
		})
		del.SetActivityStreamsActor(actorProperty)

		objectProperty := streams.NewActivityStreamsObjectProperty()
		objectProperty.AppendIRI(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log/entry/" + strconv.Itoa(id),
		})
		del.SetActivityStreamsObject(objectProperty)

		targetProperty := streams.NewActivityStreamsTargetProperty()
		targetProperty.AppendIRI(&url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log/entry/" + strconv.Itoa(newID),
		})
		del.SetActivityStreamsTarget(targetProperty)

		_, err := actor.Send(context.Background(), &url.URL{
			Scheme: "https",
			Host:   "gw2.lubar.me",
			Path:   "/log/_outbox",
		}, del)
		if err != nil {
			log.Println("error distributing log entry delete for", id, err)
			continue
		}
		log.Println("distributed delete for log entry", id, "replaced with", newID)
	}
}
