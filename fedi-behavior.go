package main

import (
	"context"
	"crypto/rsa"
	"database/sql"
	"errors"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-fed/activity/pub"
	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/streams/vocab"
	"github.com/go-fed/httpsig"
)

// pub.CommonBehavior

type fediBehavior struct {
	privKey *rsa.PrivateKey
}

func (*fediBehavior) AuthenticateGetInbox(ctx context.Context, w http.ResponseWriter, r *http.Request) (ctx_ context.Context, authenticated bool, err error) {
	// no access control for now
	return ctx, true, nil
}

func (*fediBehavior) AuthenticateGetOutbox(ctx context.Context, w http.ResponseWriter, r *http.Request) (ctx_ context.Context, authenticated bool, err error) {
	// no access control for now
	return ctx, true, nil
}

func (f *fediBehavior) NewTransport(ctx context.Context, actorBoxIRI *url.URL, gofedAgent string) (t pub.Transport, err error) {
	getSigner, _, err := httpsig.NewSigner(nil, httpsig.DigestSha256, []string{httpsig.RequestTarget, "Date", "Host"}, httpsig.Signature, 3600)
	if err != nil {
		return nil, err
	}
	postSigner, _, err := httpsig.NewSigner(nil, httpsig.DigestSha256, []string{httpsig.RequestTarget, "Date", "Digest", "Host"}, httpsig.Signature, 3600)
	if err != nil {
		return nil, err
	}
	keyURL := *actorBoxIRI
	keyURL.Path = strings.TrimSuffix(keyURL.Path, "/_outbox")
	keyURL.Path = strings.TrimSuffix(keyURL.Path, "/_inbox")
	keyURL.Fragment = keyID

	return pub.NewHttpSigTransport(debugClient, "LubarGW2/"+os.Getenv("BUILD_NUMBER")+" (+https://gw2.lubar.me)", &fediClock{}, getSigner, postSigner, keyURL.String(), f.privKey), nil
}

var debugClient = &http.Client{
	Transport: debugTransport{},
}

type debugTransport struct{}

func (debugTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	out, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		log.Printf("outgoing request:\n%s\n\nerror: %v", out, err)
		return nil, err
	}

	if resp.StatusCode >= 400 {
		in, err := httputil.DumpResponse(resp, true)
		if err != nil {
			return nil, err
		}

		if req.Method == "GET" && resp.StatusCode == 410 {
			go maybeForceUnfollow(req.URL.String())
		}

		log.Printf("outgoing request:\n%s\n\nresponse:\n%s", out, in)
	}
	return resp, nil
}

func (*fediBehavior) GetOutbox(ctx context.Context, r *http.Request) (vocab.ActivityStreamsOrderedCollectionPage, error) {
	user, ok := prefixSuffix(r.RequestURI, "/", "/_outbox")
	if !ok {
		return nil, errNotFound
	}

	query := r.URL.Query()
	before := query.Get("before")
	after := query.Get("after")
	if before != "" && after != "" {
		return nil, errors.New("either before or after can be specified, but not both")
	}

	collection := streams.NewActivityStreamsOrderedCollectionPage()
	totalItems := streams.NewActivityStreamsTotalItemsProperty()
	orderedItems := streams.NewActivityStreamsOrderedItemsProperty()

	switch user {
	case "log":
		var total int
		err := db.QueryRow(`SELECT COUNT(*) FROM "guild_log_combined"`).Scan(&total)
		if err != nil {
			return nil, err
		}
		totalItems.Set(total)
		var reverse bool
		var rows *sql.Rows
		if before != "" {
			rows, err = db.Query(`SELECT "id", "type", "time", "entries" FROM "guild_log_combined" WHERE "id" < $1 ORDER BY "id" ASC LIMIT 100`, before)
		} else if after != "" {
			rows, err = db.Query(`SELECT "id", "type", "time", "entries" FROM "guild_log_combined" WHERE "id" > $1 ORDER BY "id" DESC LIMIT 100`, after)
			reverse = true
		} else {
			rows, err = db.Query(`SELECT "id", "type", "time", "entries" FROM "guild_log_combined" ORDER BY "id" DESC LIMIT 100`)
		}
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		firstID, lastID := -1, -1

		for rows.Next() {
			var id int
			var published time.Time
			var logType, entries string
			if err := rows.Scan(&id, &logType, &published, &entries); err != nil {
				return nil, err
			}
			if firstID == -1 {
				firstID = id
			}
			lastID = id

			create := makeLogCreateActivity(id, logType, published, []byte(entries))

			if reverse {
				orderedItems.PrependActivityStreamsCreate(create)
			} else {
				orderedItems.AppendActivityStreamsCreate(create)
			}
		}
		if err = rows.Err(); err != nil {
			return nil, err
		}
		if reverse {
			firstID, lastID = lastID, firstID
		}

		prevLink := streams.NewActivityStreamsPrevProperty()
		nextLink := streams.NewActivityStreamsNextProperty()
		prevLink.SetIRI(&url.URL{
			Scheme:   "https",
			Host:     "gw2.lubar.me",
			Path:     "/log/_outbox",
			RawQuery: "after=" + strconv.Itoa(firstID),
		})
		nextLink.SetIRI(&url.URL{
			Scheme:   "https",
			Host:     "gw2.lubar.me",
			Path:     "/log/_outbox",
			RawQuery: "before=" + strconv.Itoa(lastID),
		})
		collection.SetActivityStreamsPrev(prevLink)
		collection.SetActivityStreamsNext(nextLink)
	default:
		totalItems.Set(0)
	}

	collection.SetActivityStreamsTotalItems(totalItems)
	collection.SetActivityStreamsOrderedItems(orderedItems)
	return collection, nil
}
