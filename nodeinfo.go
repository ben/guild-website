package main

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

func init() {
	http.HandleFunc("/.well-known/nodeinfo", nodeinfoHandler)
	http.HandleFunc("/nodeinfo/2.0.json", nodeinfo2Handler)
	http.HandleFunc("/api/v1/instance", v1instanceHandler)
	http.HandleFunc("/api/v1/instance/peers", v1instancePeersHandler)
}

const mastodonCompatVersion = "3.0.0"

var nodeinfoBytes = []byte(`{"links":[{"rel":"http://nodeinfo.diaspora.software/ns/schema/2.0","href":"https://gw2.lubar.me/nodeinfo/2.0.json"}]}`)

func nodeinfoHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "public, max-age=3600")

	http.ServeContent(w, r, "", time.Time{}, bytes.NewReader(nodeinfoBytes))
}

func nodeinfo2Handler(w http.ResponseWriter, r *http.Request) {
	var logCount int
	_ = db.QueryRow(`SELECT COUNT(*) FROM "guild_log_combined"`).Scan(&logCount)

	w.Header().Set("Content-Type", "application/json; profile=\"http://nodeinfo.diaspora.software/ns/schema/2.0#\"; charset=utf-8")
	w.Header().Set("Cache-Control", "public, max-age=300")
	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"metadata": map[string]interface{}{
			"features": []string{
				"guild_wars_2",
				"cute_girls",
				"trans_rights",
				"people_named_holly",
				"goddamnit_blergo",
			},
			"email":           "gw2-guild-webmaster@lubar.me",
			"nodeName":        "gw2.lubar.me",
			"nodeDescription": fullGuildName() + " Guild Wars 2 guild",
		},
		"openRegistrations": false,
		"protocols":         []string{"activitypub"},
		"services": map[string]interface{}{
			"inbound":  []string{},
			"outbound": []string{},
		},
		"usage": map[string]interface{}{
			"localPosts": logCount,
			"users": map[string]int{
				"total":          len(guildMembers) + 1,
				"activeHalfyear": 1,
				"activeMonth":    1,
			},
		},
		"software": map[string]interface{}{
			"name":    "lubargw2",
			"version": os.Getenv("BUILD_NUMBER"),
		},
		"version": "2.0",
	})
}

func v1instanceHandler(w http.ResponseWriter, r *http.Request) {
	var logCount int
	_ = db.QueryRow(`SELECT COUNT(*) FROM "guild_log_combined"`).Scan(&logCount)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "public, max-age=300")

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"version":   mastodonCompatVersion + " (compatible; LubarGW2 " + os.Getenv("BUILD_NUMBER") + ")",
		"urls":      map[string]interface{}{},
		"uri":       "https://gw2.lubar.me",
		"title":     fullGuildName(),
		"thumbnail": "https://gw2.lubar.me/logo.png",
		"stats": map[string]int{
			"user_count":   len(guildMembers) + 1,
			"status_count": logCount,
			"domain_count": len(getFederatingDomains(r.Context())),
		},
		"registrations":  false,
		"max_toot_chars": 1000,
		"languages":      []string{"en"},
		"email":          "gw2-guild-webmaster@lubar.me",
		"description":    fullGuildName() + " Guild Wars 2 guild",
	})
}

func v1instancePeersHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "public, max-age=300")

	_ = json.NewEncoder(w).Encode(getFederatingDomains(r.Context()))
}

func getFederatingDomains(ctx context.Context) []string {
	followers, err := fediDB.Followers(ctx, &url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/log",
	})
	if err != nil {
		log.Println("getFederatingDomains:", err)
		return nil
	}
	items := followers.GetActivityStreamsItems()
	if items == nil {
		return nil
	}

	seen := make(map[string]bool)
	var domains []string
	for it := items.Begin(); it != items.End(); it = it.Next() {
		iri := it.GetIRI()
		if iri == nil {
			continue
		}
		if h := iri.Hostname(); !seen[h] {
			domains = append(domains, h)
			seen[h] = true
		}
	}

	return domains
}
