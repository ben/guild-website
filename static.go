//go:generate stringer -type advisor

package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type advisor uint8

const (
	blergo advisor = iota
	brine
	castor
	gamli
	grímur
	hashim
	kensho
	kogga
	pall
	rekka
	sarah
	sophia
	numAdvisors
)

func loadStatic(filename string) []byte {
	b, err := ioutil.ReadFile(filepath.Join("static", filename))
	if err != nil {
		panic(err)
	}

	return b
}

var (
	aetheriumJS  = loadStatic("aetherium.js")
	tpPriceJS    = loadStatic("tp-price.js")
	guildCSS     = loadStatic("guild.css")
	guildLogoPNG = loadStatic("guild_logo.png")
	robotsTXT    = loadStatic("robots.txt")
	securityTXT  = loadStatic("security.txt")

	aetheriumJSIntegrity, aetheriumJSPath = handleStatic("/aetherium.js", "aetherium.js", "application/javascript; charset=utf-8", "public, max-age=86400", aetheriumJS)
	tpPriceJSIntegrity, tpPriceJSPath     = handleStatic("/tp-price.js", "tp-price.js", "application/javascript; charset=utf-8", "public, max-age=86400", tpPriceJS)
	guildCSSIntegrity, guildCSSPath       = handleStatic("/guild.css", "guild.css", "text/css; charset=utf-8", "public, max-age=86400", guildCSS)

	_, _ = handleStatic("/favicon.ico", "guild_logo.png", "image/png", "public, max-age=86400", guildLogoPNG)
	_, _ = handleStatic("/logo.png", "guild_logo.png", "image/png", "public, max-age=86400", guildLogoPNG)
	_, _ = handleStatic("/robots.txt", "robots.txt", "text/plain; charset=utf-8", "public, max-age=86400", robotsTXT)
	_, _ = handleStatic("/.well-known/security.txt", "security.txt", "text/plain; charset=utf-8", "public, max-age=86400", securityTXT)
)

func handleStatic(path, name, contentType, cacheControl string, data []byte) (integrity, path2 string) {
	hashBytes := sha256.Sum256(data)
	ext := filepath.Ext(path)
	path2 = path[:len(path)-len(ext)] + "-" + hex.EncodeToString(hashBytes[:8]) + ext
	hash := base64.StdEncoding.EncodeToString(hashBytes[:])
	etag := "\"" + hash + "\""

	http.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", contentType)
		w.Header().Set("Cache-Control", cacheControl)
		w.Header().Set("ETag", etag)
		http.ServeContent(w, r, name, time.Time{}, bytes.NewReader(data))
	})
	http.HandleFunc(path2, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", contentType)
		w.Header().Set("Cache-Control", "max-age=31536000, public, immutable")
		http.ServeContent(w, r, name, time.Time{}, bytes.NewReader(data))
	})
	return "sha256-" + hash, path2
}

var _ = registerTemplate("user-profile-page", `
<link rel="alternate" type="application/activity+json" href="">
<p>Redirecting to <a href="{{.FediverseURL}}">@{{.Fediverse}}</a>…</p>
`)

func initStatic() {
	for a := advisor(0); a < numAdvisors; a++ {
		data := loadStatic(filepath.Join("advisors", a.String()+".png"))
		_, _ = handleStatic("/advisors/"+a.String()+".png", a.String()+".png", "image/png", "public, max-age=86400", data)
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}

		http.Redirect(w, r, "/log", http.StatusFound)
	})
	http.HandleFunc("/log", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Vary", "Accept")
		var lastLogID int
		if err := db.QueryRow(`SELECT MAX("id") FROM "guild_log_combined"`).Scan(&lastLogID); err != nil {
			log.Printf("/log (HTML) %+v", err)
			http.Error(w, "internal error", http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, "/log/entry/"+strconv.Itoa(lastLogID), http.StatusFound)
	})
	http.HandleFunc("/members/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Vary", "Accept")
		w.Header().Add("Cache-Control", "private, max-age=86400")
		name := strings.TrimPrefix(r.URL.Path, "/members/")
		var member *guildMember
		for _, m := range guildMembers {
			if strings.EqualFold(name, m.Account) && m.FediverseURL != nil {
				member = &m
				w.Header().Add("Refresh", "0;url="+m.FediverseURL.String())
				break
			}
		}
		if member == nil {
			http.NotFound(w, r)
			return
		}
		renderTemplate(w, r, "user-profile-page", member, headerData{
			Title: fullGuildName() + " Member: " + member.Account,
		}, time.Time{})
	})
	http.HandleFunc("/_resize_icon/", func(w http.ResponseWriter, r *http.Request) {
		path := strings.TrimPrefix(r.URL.Path, "/_resize_icon/")
		remoteURL := "https://render.guildwars2.com/file/" + path
		resizedURL, err := gw2IconSize(64, remoteURL)
		if err != nil {
			w.Header().Add("Cache-Control", "public, max-age=3600")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		w.Header().Add("Cache-Control", "public, max-age=31536000, immutable")
		http.Redirect(w, r, resizedURL, http.StatusMovedPermanently)
	})
	http.HandleFunc("/tag/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "public, max-age=3600")
		w.WriteHeader(http.StatusNoContent)
	})
}
