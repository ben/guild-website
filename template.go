package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

var tmpl = template.New("").Funcs(template.FuncMap{
	"guild":                getGuildData,
	"gw2ico":               gw2IconSize,
	"guildCSSIntegrity":    func() string { return guildCSSIntegrity },
	"guildCSSPath":         func() string { return guildCSSPath },
	"tpPriceJSIntegrity":   func() string { return tpPriceJSIntegrity },
	"tpPriceJSPath":        func() string { return tpPriceJSPath },
	"aetheriumJSIntegrity": func() string { return aetheriumJSIntegrity },
	"aetheriumJSPath":      func() string { return aetheriumJSPath },
	"maxGuildExp":          func() int { return maxGuildExp },
	"nonce":                func() string { panic("unreachable") },
	"linkHeader":           func(string) string { panic("unreachable") },
	"lower":                strings.ToLower,
	"buildNumber":          func() string { return os.Getenv("BUILD_NUMBER") },
	"fullGuildName":        fullGuildName,
})

var _, _ = registerTemplate("header", `<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>{{.Title}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
{{if .Description -}}
<meta name="description" content="{{.Description}}">
{{end -}}
<meta name="application-name" content="LubarGW2">
<meta name="generator" content="LubarGW2 v{{buildNumber}}">
<meta name="color-scheme" content="dark">
{{printf "<%s>; rel=preload; integrity=%q; as=style" guildCSSPath guildCSSIntegrity | linkHeader -}}
<link href="{{guildCSSPath}}" integrity="{{guildCSSIntegrity}}" rel="stylesheet">
<link href="https://gw2.lubar.me/log" rel="me">
<link href="/logo.png" rel="icon">
</head>
<body>
<nav class="main-nav">
<a href="/log">Guild Log</a>
<a href="/bank">Vault</a>
<a href="/treasury">Treasury</a>
</nav>
`), registerTemplate("footer", `</body>
</html>
`)

var knownTemplates = make(map[string]*template.Template)

func registerTemplate(name, content string) string {
	if _, ok := knownTemplates[name]; ok {
		panic("duplicate registration of template \"" + name + "\"")
	}

	t := tmpl.New(name)
	knownTemplates[name] = t
	template.Must(t.Parse(content))

	return name
}

// scripts
var (
	_ = registerTemplate("aetherium.js", `{{printf "<%s>; rel=preload; integrity=%q; as=script" aetheriumJSPath aetheriumJSIntegrity | linkHeader -}}
<script src="{{aetheriumJSPath}}" integrity="{{aetheriumJSIntegrity}}" async></script>`)
	_ = registerTemplate("tp-price.js", `{{printf "<%s>; rel=preload; integrity=%q; as=script" tpPriceJSPath tpPriceJSIntegrity | linkHeader -}}
<script src="{{tpPriceJSPath}}" integrity="{{tpPriceJSIntegrity}}" async></script>`)
)
var (
	_ = registerTemplate("iconCDN", `{{linkHeader "<https://render.guildwars2.com>; rel=preconnect"}}`)

	// inline item icons
	_ = registerTemplate("icon-64", `{{template "iconCDN"}}<img src="{{gw2ico 64 .Icon}}" alt="" title="{{.Name}}" height="64" width="64" class="inline" aria-hidden="true">`)
	_ = registerTemplate("icon-32", `{{template "iconCDN"}}<img src="{{gw2ico 32 .Icon}}" srcset="{{gw2ico 64 .Icon}} 2x" alt="" title="{{.Name}}" height="32" width="32" class="inline" aria-hidden="true">`)
	_ = registerTemplate("icon-16", `{{template "iconCDN"}}<img src="{{gw2ico 16 .Icon}}" srcset="{{gw2ico 32 .Icon}} 2x, {{gw2ico 64 .Icon}} 4x" alt="" title="{{.Name}}" height="16" width="16" class="inline" aria-hidden="true">`)

	// semantically relevant item icons
	_ = registerTemplate("main-icon-64", `{{template "iconCDN"}}<img src="{{gw2ico 64 .Icon}}" alt="{{.Name}}" title="{{.Name}}" height="64" width="64">`)
	_ = registerTemplate("main-icon-32", `{{template "iconCDN"}}<img src="{{gw2ico 32 .Icon}}" srcset="{{gw2ico 64 .Icon}} 2x" alt="{{.Name}}" title="{{.Name}}" height="32" width="32">`)
	_ = registerTemplate("main-icon-16", `{{template "iconCDN"}}<img src="{{gw2ico 16 .Icon}}" srcset="{{gw2ico 32 .Icon}} 2x, {{gw2ico 64 .Icon}} 4x" alt="{{.Name}}" title="{{.Name}}" height="16" width="16">`)
)

func simpleRenderTemplate(templateName string, data interface{}) (string, error) {
	var buf bytes.Buffer

	localTmpl, err := tmpl.Clone()
	if err != nil {
		panic(err) // should never happen; programmer error if it does
	}

	localTmpl.Funcs(template.FuncMap{
		"nonce": func() string {
			return ""
		},
		"linkHeader": func(content string) string {
			return ""
		},
	})

	err = localTmpl.ExecuteTemplate(&buf, templateName, data)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}

type headerData struct {
	Title       string
	Description string
}

func renderTemplate(w http.ResponseWriter, r *http.Request, templateName string, data interface{}, header headerData, modtime time.Time) {
	var buf bytes.Buffer

	localTmpl, err := tmpl.Clone()
	if err != nil {
		panic(err) // should never happen; programmer error if it does
	}

	var links []string

	localTmpl.Funcs(template.FuncMap{
		"nonce": func() string {
			return r.Context().Value(nonceKey{}).(string)
		},
		"linkHeader": func(content string) string {
			// linear search should be fine
			for _, l := range links {
				if l == content {
					return ""
				}
			}

			links = append(links, content)
			return ""
		},
	})

	err = localTmpl.ExecuteTemplate(&buf, "header", &header)
	if err != nil {
		handleError(w, r, err)
		return
	}
	err = localTmpl.ExecuteTemplate(&buf, templateName, data)
	if err != nil {
		handleError(w, r, err)
		return
	}
	err = localTmpl.ExecuteTemplate(&buf, "footer", nil)
	if err != nil {
		handleError(w, r, err)
		return
	}

	for _, l := range links {
		w.Header().Add("Link", l)
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	http.ServeContent(w, r, "", modtime, bytes.NewReader(buf.Bytes()))
}

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	err = fmt.Errorf("%s %q: %w", r.Method, r.RequestURI, err)
	log.Printf("%+v", err)
	http.Error(w, err.Error(), http.StatusInternalServerError)
}
