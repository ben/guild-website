package main

import (
	"encoding/json"
	"log"
	"sync"
	"time"
)

var maxSize = map[int]int{
	285: 500,   // Mine Excavation 1
	331: 1500,  // Aetherium Capacity 1
	310: 3000,  // Aetherium Capacity 2
	120: 5000,  // Aetherium Capacity 3
	486: 10000, // Aetherium Capacity 4
	301: 15000, // Aetherium Capacity 5
	546: 25000, // Aetherium Capacity 6
}

var maxSizeUpgrades = []int{285, 331, 310, 120, 486, 301, 546}

var miningRate = map[int]time.Duration{
	285: 60 * time.Second, // Mine Excavation 1
	169: 50 * time.Second, // Mining Rate 1
	636: 40 * time.Second, // Mining Rate 2
	443: 30 * time.Second, // Mining Rate 3
	391: 25 * time.Second, // Mining Rate 4
	324: 20 * time.Second, // Mining Rate 5
	287: 15 * time.Second, // Mining Rate 6
}

var miningRateUpgrades = []int{285, 169, 636, 443, 391, 324, 287}

type aetheriumState struct {
	Current int
	Max     int
	Rate    time.Duration
	Last    time.Time
}

var aetheriumCache struct {
	state   aetheriumState
	lastLog int
	sync.Mutex
}

func getAetheriumState() aetheriumState {
	var lastLog int
	if err := db.QueryRow(`SELECT MAX("id") FROM "guild_log_entries"`).Scan(&lastLog); err != nil {
		panic(err)
	}

	aetheriumCache.Lock()
	state := aetheriumCache.state

	advanceTo := func(target time.Time) {
		if state.Rate == 0 {
			return
		}

		for state.Current != state.Max && state.Last.Add(state.Rate).Before(target) {
			state.Current++
			state.Last = state.Last.Add(state.Rate)
		}
	}

	if aetheriumCache.lastLog == lastLog {
		aetheriumCache.Unlock()
	} else {
		defer aetheriumCache.Unlock()

		state.Current = 0
		state.Max = 0
		state.Rate = 0
		state.Last = time.Time{}

		rows, err := db.Query(`SELECT "payload" FROM "guild_log_entries" WHERE "type" = 'upgrade' ORDER BY "id" ASC`)
		if err != nil {
			panic(err)
		}
		defer rows.Close()

		for rows.Next() {
			var entry struct {
				ID        int       `json:"id"`
				Time      time.Time `json:"time"`
				Action    string    `json:"action"`
				UpgradeID int       `json:"upgrade_id"`
			}
			var payload string
			if err := rows.Scan(&payload); err != nil {
				panic(err)
			}
			if err := json.Unmarshal([]byte(payload), &entry); err != nil {
				panic(err)
			}
			if entry.Action != "queued" {
				continue
			}
			upgrade := guildUpgrades[entry.UpgradeID]
			if upgrade == nil {
				log.Println("Missing upgrade info for upgrade", entry.UpgradeID)
				continue
			}
			rate, rateOK := miningRate[entry.UpgradeID]
			capacity, capacityOK := maxSize[entry.UpgradeID]
			aetheriumCache.lastLog = entry.ID

			if upgrade.Aetherium == 0 && !rateOK && !capacityOK {
				continue
			}

			advanceTo(entry.Time)

			if rateOK {
				state.Rate = rate
			}
			if state.Current == state.Max || capacityOK {
				state.Last = entry.Time
			}
			state.Current -= upgrade.Aetherium
			if state.Current < 0 {
				// could be because of influence boost
				state.Current = 0
			}
			if capacityOK {
				state.Max = capacity
			}
		}

		aetheriumCache.state = state
	}

	advanceTo(time.Now())

	return state
}
