#!/bin/bash -e

item_pages="`curl -I -X GET "https://api.guildwars2.com/v2/items?page=0&page_size=200&v=2019-05-03" | grep -i '^x-page-total:' | cut -d ' ' -f 2 | sed -e 's/\r//'`"

all_item_pages=()

for (( i = 0; $i < $item_pages; i++ )); do
	curl -o "items-$i.json" "https://api.guildwars2.com/v2/items?page=$i&page_size=200&v=2019-05-03"
	all_item_pages+=("items-$i.json")
	sleep 1
done

jq 'reduce inputs as $i (.; . + $i)' "${all_item_pages[@]}" > items.json

rm "${all_item_pages[@]}"

curl -o "upgrades.json" "https://api.guildwars2.com/v2/guild/upgrades?ids=all&v=2019-05-03"
