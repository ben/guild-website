package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"

	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/streams/vocab"
)

var emojisOnce sync.Once
var emojisBytes []byte

func init() {
	go emojisOnce.Do(initEmojis) // start initializing emojis early

	http.HandleFunc("/api/v1/custom_emojis", handleCustomEmojis)
}

func handleCustomEmojis(w http.ResponseWriter, r *http.Request) {
	emojisOnce.Do(initEmojis)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "public, max-age=86400")

	http.ServeContent(w, r, "", time.Time{}, bytes.NewReader(emojisBytes))
}

func initEmojis() {
	type Emoji struct {
		ShortCode       string `json:"shortcode"`
		URL             string `json:"url"`
		StaticURL       string `json:"static_url"`
		VisibleInPicker bool   `json:"visible_in_picker"`
	}

	var emojis []Emoji

	itemCache.Lock()
	for _, b := range itemCache.m {
		if b == nil {
			continue
		}
		item, err := parseItemInfo(b)
		if err != nil {
			continue
		}
		icon, err := gw2IconSize(64, item.Icon)
		if err != nil {
			continue
		}
		emojis = append(emojis, Emoji{
			ShortCode:       "gw2item_" + strconv.Itoa(item.ID),
			URL:             icon,
			StaticURL:       icon,
			VisibleInPicker: true,
		})
	}
	itemCache.Unlock()

	for _, u := range guildUpgrades {
		icon, err := gw2IconSize(64, u.Icon)
		if err != nil {
			continue
		}
		emojis = append(emojis, Emoji{
			ShortCode:       "gw2upgrade_" + strconv.Itoa(u.ID),
			URL:             icon,
			StaticURL:       icon,
			VisibleInPicker: true,
		})
	}

	var err error
	emojisBytes, err = json.Marshal(emojis)
	if err != nil {
		panic(err)
	}
}

var itemTmpl = template.Must(template.New("item").Parse(`<a href="https://wiki.guildwars2.com/index.php?title=Special:Search&search={{.ChatLink}}" rel="tag"><span aria-hidden="true">:gw2item_{{.ID}}:</span> {{.Name}}</a>`))

func getItemLink(id int) (template.HTML, itemInfo, error) {
	data, err := getItemInfo(id)
	if err != nil {
		return "", data, err
	}

	var buf bytes.Buffer
	if err = itemTmpl.Execute(&buf, &data); err != nil {
		return "", data, err
	}

	return template.HTML(buf.String()), data, nil
}

func getUpgradeEmoji(id int) (vocab.Type, error) {
	upgrade, ok := guildUpgrades[id]
	if !ok {
		return nil, fmt.Errorf("guild upgrade not found for ID %d", id)
	}

	return makeEmoji("upgrade", upgrade.Icon, upgrade.ID, upgrade.Name)
}

func getItemEmoji(id int) (vocab.Type, error) {
	data, err := getItemInfo(id)
	if err != nil {
		return nil, err
	}

	return makeEmoji("item", data.Icon, data.ID, data.Name)
}

func makeEmoji(kind, icon string, id int, name string) (vocab.TootEmoji, error) {
	icon, err := gw2IconSize(64, icon)
	if err != nil {
		return nil, err
	}

	iconUrl, err := url.Parse(icon)
	if err != nil {
		return nil, err
	}

	emoji := streams.NewTootEmoji()

	idProperty := streams.NewJSONLDIdProperty()
	idProperty.Set(&url.URL{
		Scheme: "https",
		Host:   "gw2.lubar.me",
		Path:   "/_" + kind + "/" + strconv.Itoa(id) + "/_emoji",
	})
	emoji.SetJSONLDId(idProperty)

	nameProperty := streams.NewActivityStreamsNameProperty()
	nameProperty.AppendXMLSchemaString(":gw2" + kind + "_" + strconv.Itoa(id) + ":")
	emoji.SetActivityStreamsName(nameProperty)

	iconProperty := streams.NewActivityStreamsIconProperty()
	iconImage := streams.NewActivityStreamsImage()
	iconMediaType := streams.NewActivityStreamsMediaTypeProperty()
	iconMediaType.Set("image/png")
	iconImage.SetActivityStreamsMediaType(iconMediaType)
	iconUrlProperty := streams.NewActivityStreamsUrlProperty()
	iconUrlProperty.AppendXMLSchemaAnyURI(iconUrl)
	iconImage.SetActivityStreamsUrl(iconUrlProperty)
	imageNameProperty := streams.NewActivityStreamsNameProperty()
	imageNameProperty.AppendXMLSchemaString(name)
	iconImage.SetActivityStreamsName(imageNameProperty)
	iconProperty.AppendActivityStreamsImage(iconImage)
	emoji.SetActivityStreamsIcon(iconProperty)

	return emoji, nil
}
