package main

import (
	"encoding/csv"
	"fmt"
	"net/url"
	"os"
	"reflect"
	"strings"
)

type guildMember struct {
	Fediverse    string
	FediverseURL *url.URL
	Discord      string
	Region       string
	Account      string
	APIKey       string
	Notes        string
}

var guildMembers = func() []guildMember {
	// no guild members if we're just testing templates
	if os.Getenv("DATA_SOURCE_NAME") == "" {
		return nil
	}

	f, err := os.Open("data/guild.csv")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	r := csv.NewReader(f)
	data, err := r.ReadAll()
	if err != nil {
		panic(err)
	}

	expectedHeader := []string{
		"Fediverse",
		"Discord",
		"Region",
		"Account",
		"API Key",
		"Notes",
	}
	if !reflect.DeepEqual(expectedHeader, data[0]) {
		panic(fmt.Sprintf("unexpected guild.csv header contents: %#v", data[0]))
	}
	data = data[1:]
	members := make([]guildMember, len(data))
	for i, row := range data {
		members[i].Fediverse = row[0]
		members[i].Discord = row[1]
		members[i].Region = row[2]
		members[i].Account = row[3]
		members[i].APIKey = row[4]
		members[i].Notes = row[5]

		if members[i].Fediverse != "" {
			// HACK! this should use webfinger or the URL.
			prefix := "/users/"
			parts := strings.Split(members[i].Fediverse, "@")
			if parts[1] == "lubar.me" || parts[1] == "barkshark.tk" {
				parts[1] = "mastodon." + parts[1]
			} else if parts[1] == "computerfox.xyz" {
				parts[1] = "social." + parts[1]
			} else if parts[1] == "twitter.com" {
				parts[1] = "birdsite.monster"
				prefix = "/"
			}
			members[i].FediverseURL = &url.URL{
				Scheme: "https",
				Host:   parts[1],
				Path:   prefix + parts[0],
			}
		}
	}

	return members
}()
