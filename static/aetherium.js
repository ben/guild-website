(function(){
	"use strict";

	var progress = document.getElementById("aetherium-progress");
	var current = parseInt(progress.getAttribute("data-current"), 10);
	var max = parseInt(progress.getAttribute("data-max"), 10);
	var last = parseInt(progress.getAttribute("data-last"), 10);
	var rate = parseInt(progress.getAttribute("data-rate"), 10);

	var timeToFull = document.createElement("span");
	progress.parentNode.insertBefore(timeToFull, progress.nextSibling);
	progress.parentNode.insertBefore(document.createTextNode(" "), timeToFull);

	var interval;

	// perform first update when page loads
	if (current == max) {
		timeToFull.textContent = "Full";
	} else if (document.readyState === "interactive" || document.readyState === "complete") {
		setTimeout(start, 1000 - Date.now() % 1000);
	} else {
		document.addEventListener("DOMContentLoaded", function() {
			setTimeout(start, 1000 - Date.now() % 1000);
		});
	}

	function start() {
		progress.max = rate;
		interval = setInterval(update, 1000);
		update();
	}

	function update() {
		var now = Date.now();

		// skip forward in time to now
		var changed = false;
		while (current < max && last < now - rate * 1000) {
			current++;
			last += rate * 1000;
			changed = true;
		}

		if (current === max) {
			timeToFull.textContent = "Full";
			progress.value = rate;
			clearInterval(interval);
		} else {
			var secondsToFull = (max - current) * rate - Math.round((now - last) / 1000);
			timeToFull.textContent = "";
			if (secondsToFull >= 3600) {
				var hoursToFull = Math.floor(secondsToFull / 3600);
				if (hoursToFull === 1) {
					timeToFull.textContent = "1 hour, ";
				} else {
					timeToFull.textContent = hoursToFull + " hours, ";
				}
			}
			if (secondsToFull >= 60) {
				var minutesToFull = Math.floor(secondsToFull / 60) % 60;
				if (minutesToFull === 1) {
					timeToFull.textContent += "1 minute, ";
				} else {
					timeToFull.textContent += minutesToFull + " minutes, ";
				}
			}
			secondsToFull = secondsToFull % 60;
			if (secondsToFull === 1) {
				timeToFull.textContent += "1 second remaining";
			} else {
				timeToFull.textContent += secondsToFull + " seconds remaining";
			}
			progress.value = Math.round((now - last) / 1000);
		}

		if (changed) {
			document.querySelectorAll(".aetherium progress").forEach(function(el) {
				el.value = current;
			});
			document.querySelectorAll(".aetherium .current").forEach(function(el) {
				el.textContent = current;
			});
		}
	}
})();
