(function() {
	"use strict";

	var toFind = [].map.call(document.querySelectorAll(".tp-price"), function(el) {
		return el.getAttribute("data-item-id");
	}).filter(function(v, i, a) {
		return a.indexOf(v) === i;
	});

	if (toFind.length) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "https://api.guildwars2.com/v2/commerce/prices?v=2019-05-03&ids=" + toFind.join(","));
		xhr.responseType = "json";
		xhr.onload = function() {
			xhr.response.forEach(function(item) {
				document.querySelectorAll(".tp-price[data-item-id=\"" + parseInt(item.id, 10) + "\"]").forEach(function(el) {
					var vendorPrice = el.getAttribute("data-vendor-price");
					var vendorCount = el.getAttribute("data-vendor-count") || 1;
					if (!item.sells.quantity || (vendorPrice && item.sells.unit_price >= parseInt(vendorPrice, 10) / vendorCount)) {
						if (el.previousElementSibling.nodeName === "BR") {
							el.parentNode.removeChild(el.previousElementSibling);
						}
						el.parentNode.removeChild(el);
						return;
					}

					el.textContent = item.sells.quantity.toLocaleString() + " sell orders from " + formatCoins(item.sells.unit_price) + ".";
				});
			});
		};
		xhr.send();
	}

	function formatCoins(copper) {
		var text = [];
		if (copper >= 10000) {
			var gold = Math.floor(copper / 10000);
			copper = copper % 10000;
			text.push(gold.toLocaleString(), "gold");
		}
		if (copper >= 100) {
			var silver = Math.floor(copper / 100);
			copper = copper % 100;
			text.push(silver, "silver");
		}
		if (copper >= 1 || !text.length) {
			text.push(copper, "copper");
		}
		return text.join(" ");
	}
})();
