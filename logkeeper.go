package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"time"
)

func watchGuildLog() {
	if _, err := db.Exec(`
CREATE TABLE IF NOT EXISTS "guild_log_entries" (
	"id" BIGINT NOT NULL PRIMARY KEY,
	"time" TIMESTAMPTZ NOT NULL,
	"type" TEXT NOT NULL,
	"user" TEXT NOT NULL,
	"payload" JSONB NOT NULL
)
`); err != nil {
		panic(err)
	}

	if _, err := db.Exec(`
CREATE TABLE IF NOT EXISTS "guild_log_blacklist" (
	"id" BIGINT NOT NULL PRIMARY KEY
	            REFERENCES "guild_log_entries"("id")
		    ON DELETE CASCADE
)
`); err != nil {
		panic(err)
	}

	if _, err := db.Exec(`
CREATE MATERIALIZED VIEW IF NOT EXISTS
"guild_log_combined" ("id", "type", "time", "user", "entries") AS
WITH "donation_bounds" AS (
	SELECT e.*,
	       CASE WHEN ("time" - LAG("time")
	                  OVER (PARTITION BY "user", ("type" = 'stash')
	                        ORDER BY "time")) < INTERVAL '5 minutes'
	            THEN 0 ELSE 1 END "is_bound"
	  FROM "guild_log_entries" e
	  LEFT JOIN "guild_log_blacklist" bl
	         ON e."id" = bl."id"
	 WHERE bl."id" IS NULL
	   AND ("type" = 'treasury'
	     OR "type" = 'stash'
	     OR ("type" = 'upgrade'
	    AND  "payload"->>'action' = 'completed'))
), "donation_groups" as (
	SELECT *,
	       SUM("is_bound")
	       OVER (PARTITION BY "user", ("type" = 'stash')
	             ROWS BETWEEN UNBOUNDED PRECEDING
	                      AND CURRENT ROW) "grp_index"
	  FROM "donation_bounds"
	 ORDER BY "time"
), "donations" as (
	SELECT *,
	       MAX("id")
	       OVER (PARTITION BY "user", ("type" = 'stash'), "grp_index") "grp"
	  FROM "donation_groups"
)
SELECT MAX("id") "id",
       'donation' "type",
       MAX("time") "time",
       "user",
       jsonb_agg("payload") "entries"
  FROM "donation_groups"
WHERE "type" <> 'stash'
GROUP BY "user", "grp_index"

UNION ALL

SELECT MAX("id") "id",
       'stash' "type",
       MAX("time") "time",
       "user",
       jsonb_agg("payload") "entries"
  FROM "donation_groups"
WHERE "type" = 'stash'
GROUP BY "user", "grp_index"

UNION ALL

SELECT l."id",
       "type",
       "time",
       "user",
       jsonb_build_array("payload") "entries"
  FROM "guild_log_entries" l
  LEFT JOIN "guild_log_blacklist" bl
         ON l."id" = bl."id"
 WHERE bl."id" IS NULL
   AND CASE
       WHEN "type" = 'treasury' THEN
            FALSE
       WHEN "type" = 'stash' THEN
            FALSE
       WHEN "type" = 'influence' THEN
            FALSE
       WHEN "type" = 'upgrade' THEN
            ("payload" ? 'upgrade_id') AND NOT EXISTS(
            SELECT 1
              FROM "guild_log_entries" d
             WHERE d."type" = 'upgrade'
               AND ((d."payload"->>'action') = 'completed'
                OR  (d."payload" ? 'count'))
               AND (d."payload"->>'upgrade_id') = (l."payload"->>'upgrade_id'))
       ELSE
            TRUE
       END;
`); err != nil {
		panic(err)
	}

	if _, err := db.Exec(`
CREATE UNIQUE INDEX IF NOT EXISTS "guild_log_combined_pkey"
ON "guild_log_combined"("id");
`); err != nil {
		panic(err)
	}

	for {
		time.Sleep(30 * time.Second)
		updateGuildLog()
	}
}

func updateGuildLog() {
	apiArgs := url.Values{
		"access_token": {leaderAPIKey},
		"v":            {arbitraryAPIVersionDate},
	}

	resp, err := http.Get("https://api.guildwars2.com/v2/guild/" + guildID + "/log?" + apiArgs.Encode())
	if err != nil {
		log.Println("Updating guild log failed:", err)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Println("Updating guild log failed:", resp.Status)
		return
	}

	var entries []json.RawMessage
	if err = json.NewDecoder(resp.Body).Decode(&entries); err != nil {
		log.Println("Updating guild log failed:", err)
		return
	}

	var toDistribute []int

	for _, payload := range entries {
		var meta struct {
			ID   int64     `json:"id"`
			Time time.Time `json:"time"`
			Type string    `json:"type"`
			User string    `json:"user"`
		}

		if err = json.Unmarshal([]byte(payload), &meta); err != nil {
			log.Println(err)
			continue
		}

		var one int
		if err = db.QueryRow(`SELECT 1 FROM "guild_log_entries" WHERE "id" = $1`, meta.ID).Scan(&one); err == nil {
			// already in the database
			continue
		} else if err != sql.ErrNoRows {
			log.Println(err)
			continue
		}

		if _, err = db.Exec(`INSERT INTO "guild_log_entries" ("id", "time", "type", "user", "payload") VALUES($1, $2, $3, $4, $5)`, meta.ID, meta.Time, meta.Type, meta.User, string(payload)); err != nil {
			log.Println(err)
			continue
		}

		toDistribute = append(toDistribute, int(meta.ID))
	}

	if len(toDistribute) == 0 {
		return
	}

	if _, err = db.Exec(`REFRESH MATERIALIZED VIEW CONCURRENTLY "guild_log_combined"`); err != nil {
		log.Println(err)
		return
	}

	log.Println("distributing new log entries:", toDistribute)
	go distributeLogEntries(toDistribute)
}
